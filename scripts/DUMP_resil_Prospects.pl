#! /usr/bin/perl

#use strict;
use DBI;
use FTprivdsl;
use Date::Simple;

my $DBtableD = "DBI:mysql:degroup;172.20.241.4";
my $DBloginD = 'saisie';
my $DBpasswdD = 'youpalalo';

my $today = Date::Simple::today();
my $zedate = Date::Simple::today() - 60;
my $years = $zedate - 365 * 3;

my $sth;

  my %desc_ope = (
    'W' => 'Orange',
    '9' => 'SFR / Neufbox',
    'O' => 'Bouygues Telecom / Bbox',
    'X' => 'Autre',
    '0' => 'Pas encore choisi',
  );

my $dbh = DBI->connect($DBtableD, $DBloginD, $DBpasswdD);


my $campagne = "RESF_0000001_".$today->as_d8;
if (-f "/root/degroup/vaillante/file/$campagne") {
    die "ALREADY $campagne\n";
}
open (OUT, ">/root/degroup/vaillante/file/$campagne") || die;
binmode(OUT, ':encoding(UTF-8)');

print OUT "ID_CONTACT_SRC|SRC|TYPE_SRC|CODE_CAMPAGNE|CIV|NOM|PRENOM|ADR1|ADR2|ADR3|ADR4|ID_ADR_FREE|CP|VILLE|PAYS|TEL1|TEL2|CODE_INSEE|HEXACLE|DATE_ELIGIBILITEFTTH|NRO|LIBELLE_OFFRE_MOBILE|DATE_DE_CREATION_CONTRAT_MOBILE|ANCIENNETE_ELI_FTTH|EMAIL|OPTIN_APPEL|OPTIN_SMS|OPTIN_COURRIER|OPTIN_EMAIL|DT_DEB_DROIT_ADR|DT_FIN_DROIT_ADR|DT_DEB_DROIT_TEL1|DT_FIN_DROIT_TEL1|DT_DEB_DROIT_TEL2|DT_FIN_DROIT_TEL2|DT_DEB_DROIT_EMAIL|DT_FIN_DROIT_EMAIL\n";


my $liste_email;
my $count  = 0;

print "$zedate / $years\n";
$sth = $dbh->prepare("SELECT id_client, DATE_FORMAT(date_saisie,'%Y%m%d') as date_s, prenom_abonne, nom_abonne, code_postal, DATE_FORMAT(date_resil,'%Y%m%d') AS date_f, type_offre, tel, email, societe, adresse, voie, ville, code_insee, tel_portable  FROM resil WHERE date_resil<'$zedate' AND date_resil>'$years' and id_presta=1 and type_offre in (1,3,5,20);");
$sth->execute();
$sth->bind_columns(\($id_client, $date_saisie, $prenom_abonne, $nom_abonne, $cp, $date_resil, $type_offre, $tel, $email, $societe, $adresse, $voie, $ville, $code_insee, $tel_portable));
while ($sth->fetch()) {

	$id_dem = $id_new = '';	
	$substh = $dbh->prepare("SELECT id_new FROM demenagement WHERE id_client='$id_client';");
	$substh->execute();
	$substh->bind_columns(\($id_new));
	$substh->fetch();
	$substh->finish();
	$id_dem = 'DEM '.$id_new if ($id_new);

	$id_adresse ='';
	$substh = $dbh->prepare("SELECT id_adresse FROM adresse_fibre WHERE id_client='$id_client';");
	$substh->execute();
	$substh->bind_columns(\($id_adresse));
	$substh->fetch();
	$substh->finish();

	$liste_email = '';
    $substh = $dbh->prepare("SELECT id_client FROM liste_email WHERE id_client='$id_client' AND flag in (10);");
    $substh->execute();
    $substh->bind_columns(\$liste_email);
    $substh->fetch();
    $substh->finish();
	my $no_mail = '1';
	$no_mail = '0' if ($liste_email == $id_client) ;

#	$motif = $ope = $date_pre= '';	
#	$substh = $dbh->prepare("SELECT motif, ope, date, flag FROM pre_resil WHERE id_client='$id_client' order by date DESC LIMIT 1;");
#	$substh->execute();
#	$substh->bind_columns(\($motif, $ope, $date_pre, $flag_pre));
#	$substh->fetch();
#	$substh->finish();
#	if ($flag_pre == 1) {
#		$flag_pre = "immediate";
#	}
#	elsif ($flag_pre == 2) {
#		$flag_pre = "sous 10 jours";
#	}
#	else {
#		$flag_pre = "fin de mois";
#	}

	if ($societe =~ /"/) {
		$societe =~ s/"/""/g;
		$societe = '"'.$societe.'"';
	}

	if ($voie =~ /"/) {
		$voie =~ s/"/""/g;
		$voie = '"'.$voie.'"';
	}

	if ($adresse =~ /"/) {
		$adresse =~ s/"/""/g;
		$adresse = '"'.$adresse.'"';
	}

	if ($nom_abonne =~ /\|/) {
		$nom_abonne =~ s/\|/,/g;
	}

	if ($prenom_abonne =~ /\|/) {
		$prenom_abonne =~ s/\|/,/g;
	}
	if ($societe =~ /\|/) {
		$societe =~ s/\|/,/g;
	}



	print OUT "$id_client|RESF|Interne|||$nom_abonne|$prenom_abonne|$societe|$adresse|$voie||$id_adresse|$cp|$ville|||$tel_portable|$code_insee|||||$date_saisie||$email||||$no_mail||||||||\n";
	$count ++;

}
$sth->finish();
$dbh->disconnect;

close OUT;
$count ++;

rename "/root/degroup/vaillante/file/$campagne", "/root/degroup/vaillante/file/$campagne"."_V1_$count.csv"

