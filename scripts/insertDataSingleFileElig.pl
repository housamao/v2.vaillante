#!/usr/bin/perl

use strict;
use utf8;
use DBI;

binmode( STDOUT, ":utf8" );

########## DEV ##########

my $DBtable  = "DBI:mysql:vaillante;127.0.0.1";
my $DBlogin  = 'root';
my $DBpasswd = 'coucou12345';

########## END DEV ##########
# my $DBtable  = "DBI:mysql:vaillante;127.0.0.1";
# # my $DBtable  = "DBI:mysql:vaillante;172.20.241.231";
# my $DBlogin  = 'hossen';
# my $DBpasswd = undef;

my $dbh = DBI->connect(
    $DBtable, $DBlogin,
    $DBpasswd,
    {   RaiseError        => 1,
        mysql_enable_utf8 => 1
    }
);

my $value;
my $row;
my $i          = 0;
my $dataInsert = 0;
my $count      = 0;
my $batchSize  = 500;
my $total = 0;
my @values;
my @dataBatch;
my $values_str;
my $query = <<SQL;
        INSERT INTO tmp_ref_eligibilite SET 
            id_adr_free = ?,
            adresse_num_adresse_suffixe = ?,
            voie_code_fantoir = ?,
            voie_code_insee = ?,
            voie = ?,
            gestionnaire_nom = ?,
            operateur = ?,
            zone = ?,
            pm_nom = ?,
            techno = ?,
            nro = ?,
            date_elig = ?,
            date_j3m = ?
SQL

$dbh->{'AutoCommit'} = 0;

$total++ while <>;

my $sth = $dbh->prepare($query);

while ( my $row = <> ) {
    my @rowTemp = split( /;/, $row );
    push @dataBatch, [ @rowTemp[ 0 .. 12 ] ];
}
foreach my $enregistrement (@dataBatch) {
    push @values, $enregistrement;
    $count++;
    if (   $count % $batchSize == 0
        || $count == $total )
    {
        my $values_str = join( ", ", @values );
        try {

            $sth->execute(@$_) for @values;
            @values = ();

            $dataInsert = $count;

        }
        catch {
            print "[$i] got dbi error: $_";
        };
    }
    $i++;
    if ( $i % 10000 eq 0 ) {
        $dbh->commit();
    }
}

$sth->finish;

print "$dataInsert / $i nouvelles lignes sont enregistrees dans la BDD\n";
$dbh->commit();

