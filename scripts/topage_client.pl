#! /usr/bin/perl

#use strict;
use DBI;
use FTprivdsl;
use Date::Simple;

my $DBtableD = "DBI:mysql:degroup;172.20.241.4";
my $DBloginD = 'saisie';
my $DBpasswdD = 'youpalalo';
my $dbh = DBI->connect($DBtableD, $DBloginD, $DBpasswdD);

my $DBtableP = "DBI:Pg:dbname=free;host=172.20.241.231";
my $DBloginP = 'free';
my $DBpasswdP = 'free';
my $dbh_P = DBI->connect($DBtableP, $DBloginP, $DBpasswdP);

my $sth;
my $substh;
my ($tel1, $tel2, $email);

$sth = $dbh_P->prepare("SELECT id_contact, prenom, nom, id_adr, id_tel_1, id_tel_2, id_email, top_client, date_top_client, orig_top_client FROM contact WHERE top_client IS NULL AND nom IS NOT NULL and prenom IS NOT NULL;");
$sth->execute();
while ($res = $sth->fetchrow_hashref ) {

#   print "$res->{id_contact};$res->{date_top_client};$res->{id_tel_1};$res->{id_tel_2};$res->{id_email}\n";
    $tel1 = $dbh_P->selectrow_hashref("SELECT * FROM phone WHERE id_phone = '$res->{id_tel_1}';") if ($res->{id_tel_1});
    $tel2 = $dbh_P->selectrow_hashref("SELECT * FROM phone WHERE id_phone = '$res->{id_tel_2}';") if ($res->{id_tel_2});

    my $top = '';
    if ($tel1->{tel_clean}) {   
        $top = $dbh->selectrow_hashref("SELECT id FROM users WHERE tel_portable = '$tel1->{tel_clean}' AND flag in (100,108,109);");
        if ($top) {
            print "TOP tel1;$tel1->{tel_clean};$top->{id}\n";
            print "UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_tel' WHERE id_contact='$res->{id_contact}';\n";
            $dbh_P->do("UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_tel' WHERE id_contact='$res->{id_contact}';");
            next;
        }
    }
    elsif ($tel2->{tel_clean}) {    
        $top = $dbh->selectrow_hashref("SELECT id FROM users WHERE tel_portable = '$tel2->{tel_clean}' AND flag in (100,108,109);");
        if ($top) {
            print "TOP tel2;$tel2->{tel_clean};$top->{id}\n";
            print "UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_tel' WHERE id_contact='$res->{id_contact}';\n";
            $dbh_P->do("UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_tel' WHERE id_contact='$res->{id_contact}';");
            next;
        }
    }

    $email = '';
    $email = $dbh_P->selectrow_hashref("SELECT * FROM email WHERE id_email = '$res->{id_email}';") if ($res->{id_email} > 1);
    
    if ($email->{email}) {  
        $top = $dbh->selectrow_hashref("SELECT id FROM users WHERE email = '$email->{email}' AND flag in (100,108,109);");
        if ($top) {
            print "TOP email;$email->{email};$top->{id}\n";
            print "UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_email' WHERE id_contact='$res->{id_contact}';\n";
            $dbh_P->do("UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_email' WHERE id_contact='$res->{id_contact}';");
            next;
        }
    }
    
    $adresse = $dbh_P->selectrow_hashref("SELECT * FROM adresse WHERE id_adr = '$res->{id_adr}';") if ($res->{id_adr});

#   print "$res->{id_contact};$res->{date_top_client};$res->{nom};$res->{prenom};$adresse->{id_adr_free};$adresse->{code_insee};$adresse->{zip_code}\n";

    if ($adresse->{id_adr_free}) {  
        $top = $dbh->selectrow_hashref("SELECT id FROM users WHERE nom_abonne = ".$dbh->quote($res->{nom})." AND prenom_abonne=".$dbh->quote($res->{prenom})." AND idvoie='$adresse->{id_adr_free}' AND flag in (100,108,109);");
        if ($top) {
            print "TOP id_adr;$adresse->{id_adr_free};$top->{id}\n";
            print "UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_id_adr' WHERE id_contact='$res->{id_contact}';\n";
            $dbh_P->do("UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_id_adr' WHERE id_contact='$res->{id_contact}';");
            next;
        }
    }

    if ($adresse->{code_insee}) {   
        $top = $dbh->selectrow_hashref("SELECT id FROM users WHERE nom_abonne = ".$dbh->quote($res->{nom})." AND prenom_abonne=".$dbh->quote($res->{prenom})." AND code_insee='$adresse->{code_insee}' AND flag in (100,108,109);");
        if ($top) {
            print "TOP code_insee;$adresse->{code_insee};$top->{id}\n";
            print "UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_insee' WHERE id_contact='$res->{id_contact}';\n";
            $dbh_P->do("UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_insee' WHERE id_contact='$res->{id_contact}';");
            next;
        }
    }
    
    if ($adresse->{cp_orig}) {  
        $top = $dbh->selectrow_hashref("SELECT id FROM users WHERE nom_abonne = ".$dbh->quote($res->{nom})." AND prenom_abonne=".$dbh->quote($res->{prenom})." AND code_postal='$adresse->{cp_orig}' AND flag in (100,108,109);");
        if ($top) {
            print "TOP CP;$adresse->{cp_orig};$top->{id}\n";
            print "UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_CP' WHERE id_contact='$res->{id_contact}';\n";
            $dbh_P->do("UPDATE contact SET top_client=true, date_top_client=NOW(), orig_top_client='FREE_CP' WHERE id_contact='$res->{id_contact}';");
            next;
        }
    }

    $dbh_P->do("UPDATE contact SET top_client=false, date_top_client=NOW(), orig_top_client='FREE' WHERE id_contact='$res->{id_contact}';");

#   print "$tel1->{tel_clean};$tel1->{type}\n";
#   print "$tel2->{tel_clean};$tel2->{type}\n";
#   print "$email->{email}\n";

}
$sth->finish();
$dbh_P->disconnect;

