#!/usr/bin/perl

use strict;
use utf8;
use Text::ParseWords;

binmode( STDOUT, ":utf8" );

my $ch;
my @ar;

while ( $ch = <> ) {

    $ch =~ s/"//g;
    @ar = split( /;/, $ch );

    # @ar = Text::ParseWords::parse_line(';', 0, $ch);

    $ar[6]  =~ s/^operateur=//;
    $ar[7]  =~ s/^zone=//;
    $ar[8]  =~ s/^techno=//;
    $ar[10] =~ s/^nro=//;
    # $ar[12] =~ s/(?<!\d-\d{2}-\d{4})[^\n;]+\K(?<!;)(?=\n|\z)/;/g; # Ajoute un point virgule a la fin sauf si il existe deja
    $ar[12] =~ s/(?<!\d-\d{2}-\d{4})[^\n|]+\K(?:(?<=\|)(?=\n|\z)|(?<=\n)\|(?=\z))/ /g; # Ajoute un PIPE a la fin sauf si il existe deja
    # $ar[12] =~ s/(?<!\d-\d{2}-\d{4})[^\n;]+\K(?:(?<=;)(?=\n|\z)|(?<=\n);(?=\z))/ /g; # Supprime le point virgule a la fin du mot

    print join( '|', @ar );
    

}