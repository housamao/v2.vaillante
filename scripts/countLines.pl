#!/usr/bin/perl

use warnings;
use strict;
use Benchmark;
use Data::Dumper;

print "Counting lines in files\n";

my $t0 = new Benchmark;

my $file = @ARGV;
my $cnt;


$cnt++ while <$file>;

print "$cnt\n";

my $t1 = new Benchmark;
my $td = timediff( $t1, $t0 );
print "DONE!!\n", timestr($td), " seconds\n";
