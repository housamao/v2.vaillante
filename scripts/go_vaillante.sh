#! /bin/bash

DATE=`date -d '1 day ago' +%Y%m%d`
TODAY=`date -d 'today' +%Y%m%d`

CHEMIN="/home/aid/data/in/REFERENTIELS/ref_eligibilite"

scp hossen@172.20.241.29:/root/degroup/fibre/adresses_elig_${DATE}_08.csv hossen@vaillante:/home/aid/data/in/REFERENTIELS/ref_eligibilite/adresses_elig_week.csv
scp hossen@172.20.241.29:/root/degroup/fibre/adresses_elig_raco_${DATE}_08.csv hossen@vaillante:/home/aid/data/in/REFERENTIELS/ref_eligibilite/adresses_elig_raco_week.csv

ssh hossen@vaillante "/usr/local/bin/normalize.pl $CHEMIN/adresses_elig_week.csv > $CHEMIN/adresses_elig_week.N.csv
/usr/local/bin/normalize.pl $CHEMIN/adresses_elig_raco_week.csv > $CHEMIN/adresses_elig_raco_week.N.csv
rm -v $CHEMIN/adresses_elig_week.csv
rm -v $CHEMIN/adresses_elig_raco_week.csv
"

ssh hossen@vaillante "mysql 'host=127.0.0.1 user=root password=coucou12345 dbname=vaillante' < /usr/local/bin/InsertTableRef_eligibilite.sql"