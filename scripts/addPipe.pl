#!/usr/bin/perl

use strict;
use utf8;

binmode( STDOUT, ":utf8" );

my $ch;
my @ar;

while ( $ch = <> ) {

    $ch .= "|";
    @ar = split( /\|/, $ch );

    print join( '|', @ar );
    
}