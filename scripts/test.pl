use Text::CSV;

$input_file = "/home/hossen/ref_eligibilite/adresses_elig_raco_week.csv";
$output_file = "test.csv";

if (! open ENTREE, "<", $input_file) {
   print "Could not open handle ENTREE for $input_file. $!\n";
}
if (! open SORTIE, ">", $output_file) {
   print "Could not open handle SORTIE for $output_file. $!\n";
}

$start = time();

my $csv_reader = Text::CSV->new();
my @columns;

while (<ENTREE>) {
   $csv_reader->parse($_);
   @columns = $csv_reader->fields();
   print SORTIE "$columns[2]\t$columns[0]\n";
}

$end = time();
$duration = $end - $start;

print "Reading took $duration seconds.\n";

close ENTREE;
close SORTIE;