package vtl_db;

use strict;
use warnings;
use DBI;
use utf8;
use Encode qw(encode_utf8);

my $DBtable;
my $DBlogin;
my $DBpasswd;

# GET DB

sub get_db {
    my ($this) = @_;

    my $dbh;

    # if ( $ENV{SERVER_NAME} eq 'localhost' ) {
    ######### DEV ##########

    $DBtable  = "DBI:mysql:vaillante;127.0.0.1";
    $DBlogin  = 'root';
    $DBpasswd = 'coucou12345';

    ######### END DEV ##########
    # }
    # else {

    # $DBtable  = "DBI:mysql:vaillante;127.0.0.1";
    # $DBlogin  = 'hossen';
    # $DBpasswd = undef;

    # }

    $dbh = $this->connect_to_bdd( $DBtable, $DBlogin, $DBpasswd );
    return $dbh;
}

sub get_db_free {
    my ($this) = @_;

    my $dbh;

    # if ( $ENV{SERVER_NAME} eq 'localhost' ) {
    ########## DEV ##########

    $DBtable  = "DBI:mysql:degroup;127.0.0.1";
    $DBlogin  = 'root';
    $DBpasswd = 'coucou12345';

    ########## END DEV ##########
    # }
    # else {

    # $DBtable  = "DBI:mysql:degroup;172.20.241.8";
    # $DBlogin  = 'vaillante';
    # $DBpasswd = 'AePh8uphaXuo';

    # }

    $dbh = $this->connect_to_bdd( $DBtable, $DBlogin, $DBpasswd );
    return $dbh;
}

sub connect_to_bdd {
    my ( $this, $DBtable, $DBlogin, $DBpasswd ) = @_;

    my $dbh = DBI->connect(
        $DBtable, $DBlogin,
        $DBpasswd,
        {   RaiseError        => 1,
            mysql_enable_utf8 => 1
        }
    );

    return $dbh;
}

#############
#           #
#  SELECT   #
#           #
#############

sub select_test_push_to_ftp {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT id_contact_src, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, top_client, date_top_client, orig_top_client, optin_appel, optin_sms, optin_courrier, optin_email, operateur, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd, techno FROM contact WHERE id_file = 1 AND top_client = 0 AND state != 7;
SQL

    return $query;

}
sub select_all_data_group_by_cp {
    my ($this) = @_;
    my $query = <<SQL;
        SELECT cp, count(cp) as nbr_cp, sum(optin_appel) as nbr_optin_appel, sum(optin_email) as nbr_optin_email, sum(optin_sms) as nbr_optin_sms, sum(optin_courrier) as nbr_optin_courrier FROM contact WHERE date_eligibilite_ftth >= DATE_SUB(now(), INTERVAL 1 MONTH) GROUP BY cp, optin_appel, optin_email, optin_sms, optin_courrier LIMIT 10
SQL

    return $query;
}

sub select_all_data_with_date_elig {
    my ($this) = @_;
    my $query = <<SQL;
        SELECT id_contact_src, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, top_client, date_top_client, orig_top_client, optin_appel, optin_sms, optin_courrier, optin_email, operateur, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd, techno FROM contact WHERE date_eligibilite_ftth >= DATE_SUB(now(), INTERVAL 21 DAY) AND is_abo_box = 0;
SQL

    return $query;
}

sub select_all_data_fresh_date_elig {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT id_contact_src, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, top_client, date_top_client, orig_top_client, optin_appel, optin_sms, optin_courrier, optin_email, operateur, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd, techno FROM contact WHERE date_eligibilite_ftth >= DATE_SUB(now(), INTERVAL 10 DAY) AND is_abo_box = 0;
SQL

    return $query;
}

sub select_all_data_fresh_date_elig_table {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT c.id_contact_src, c.src, c.type_src, c.code_campagne, c.civ, c.nom, c.prenom, c.adr1, c.adr2, c.adr3, c.adr4, c.id_adr_free, c.cp, c.ville, c.pays, c.tel1, c.tel2, c.code_insee, c.hexacle, c.date_eligibilite_ftth, c.nro, c.libelle_offre_mobile, c.date_de_creation_contrat_mobile, c.anciennete_eli_ftth, c.email, c.top_client, c.date_top_client, c.orig_top_client, c.optin_appel, c.optin_sms, c.optin_courrier, c.optin_email, c.operateur, c.dt_deb_droit_adr, c.dt_fin_droit_adr, c.dt_deb_droit_tel1, c.dt_fin_droit_tel1, c.dt_deb_droit_tel2, c.dt_fin_droit_tel2, c.dt_deb_droit_email, c.dt_fin_droit_email, c.id_rgpd, c.techno FROM contact as c INNER JOIN ref_eligibilite ON c.id_adr_free = ref_eligibilite.id_adr_free WHERE c.dt_fin_droit_tel1 <= DATE_SUB(now(), INTERVAL 3 MONTH);
SQL

    return $query;
}

sub select_topage_client_unset {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT id_contact_src, nom, prenom, tel1, tel2, id_adr_free, cp, code_insee, email FROM contact WHERE orig_top_client IS NULL ORDER BY id DESC LIMIT 10000
SQL

    return $query;
}

sub select_topage_client_exist {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT u.id, u.tel_portable, u.nom_abonne, u.prenom_abonne, u.email, af.id_adresse, u.code_insee, u.code_postal FROM users as u INNER JOIN adresse_fibre as af ON u.id = af.id_client WHERE u.nom_abonne =  ? AND u.prenom_abonne = ? AND af.id_adresse = ? AND u.flag IN (100,108,109);
SQL

    return $query;

}

sub select_all_tmp_contact {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT id_contact_src, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, top_client, date_top_client, orig_top_client, optin_appel, optin_sms, optin_courrier, optin_email, operateur, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd, techno FROM tmp_contact WHERE top_client = 0 AND state != 7;
SQL

    return $query;

}

#############
#           #
#  UPDATE   #
#           #
#############

sub update_file {
    my ($this) = @_;

    my $query = <<SQL;
    UPDATE file SET abo_free = (SELECT count(id) FROM tmp_contact WHERE is_abo_box = 1), resil = (SELECT count(id) FROM tmp_contact WHERE state = 7), status = 4 ORDER BY id DESC LIMIT 1;
SQL

    return $query;
}

sub update_file_feedback {
    my ($this) = @_;

    my $query = <<SQL;
    UPDATE file_feedback SET vente = (SELECT count(id) FROM tmp_contact_feedback WHERE state = 9), refus = (SELECT count(id) FROM tmp_contact_feedback WHERE flag = 4 OR flag = 10), status = 4 ORDER BY id DESC LIMIT 1;
SQL

    return $query;
}

sub update_data_with_id_file {
    my ($this) = @_;

    my $query = <<SQL;
    UPDATE tmp_contact SET id_file = (SELECT id FROM file ORDER BY id DESC LIMIT 1);
SQL

    return $query;
}

sub update_data_with_id_file_feedback {
    my ($this) = @_;

    my $query = <<SQL;
    UPDATE tmp_contact_feedback SET id_file = (SELECT id FROM file_feedback ORDER BY id DESC LIMIT 1);
SQL

    return $query;
}

################### REQUETE TMP ############################ 

sub tmp_update_topage_client {
    my ($this) = @_;

    my $query = <<SQL;
        UPDATE tmp_contact SET top_client = 1, date_top_client = ?, orig_top_client = ?, email = ?, tel2 = ?, is_abo_box = 1 WHERE id_adr_free = ? AND nom = ? AND prenom = ?;
SQL

    return $query;
}

sub tmp_update_flag_feedback_unset {
    my ($this) = @_;

    my $query = <<SQL;
        UPDATE tmp_contact_feedback SET flag = ? WHERE id_contact_src = ?;
SQL

    return $query;
}

sub tmp_update_flag_feedback_vente {
    my ($this) = @_;

    my $query = <<SQL;
        UPDATE tmp_contact_feedback SET state = 9 WHERE qualif_lvl1 LIKE "%Vente OK%" AND state != 9;
SQL

    return $query;
}

sub tmp_update_flag_feedback_no_contact {
    my ($this) = @_;

    my $query = <<SQL;
        UPDATE tmp_contact_feedback SET state = 3 WHERE qualif_lvl2 LIKE "%Injoignable permanent%" AND state != 3;
SQL

    return $query;
}

sub tmp_update_flag_feedback_no_contact_anymore {
    my ($this) = @_;

    my $query = <<SQL;
        UPDATE tmp_contact_feedback SET state = 2 WHERE qualif_lvl2 LIKE "%Ne veut plus être contacté%" AND state != 2;
SQL

    return $query;
}

sub tmp_update_flag_feedback_not_interested {
    my ($this) = @_;

    my $query = <<SQL;
        UPDATE tmp_contact_feedback SET state = 4 WHERE qualif_lvl2 LIKE "%Ne veut pas répondre%" AND state != 4;
SQL

    return $query;
}

sub tmp_update_flag_feedback_bad_number {
    my ($this) = @_;

    my $query = <<SQL;
        UPDATE tmp_contact_feedback SET state = 5 WHERE qualif_lvl2 LIKE "%Faux numéro%" AND state != 5;
SQL

    return $query;
}

sub tmp_update_flag_feedback_is_abo {
    my ($this) = @_;

    my $query = <<SQL;
        UPDATE tmp_contact_feedback SET state = 10 WHERE qualif_lvl2 LIKE "%déjà abonné%" AND state != 10;
SQL

    return $query;
}

sub tmp_update_table_bookers_from_adresse {
    my ($this) = @_;

# INSIDE THE SCRIPT TO INSERT
# UPDATE tmp_contact JOIN adresse ON tmp_contact.hexacle = adresse.adresse_src_id SET tmp_contact.id_adr_free = adresse.adresse_id, tmp_contact.voie_id = adresse.voie_id, tmp_contact.operateur = adresse.operateur, tmp_contact.date_eligibilite_ftth = DATE_SUB(adresse.date_debut_commercialisation, INTERVAL 5 DAY), tmp_contact.nro = adresse.code_nro, tmp_contact.flag = 2 WHERE tmp_contact.flag = 1 ORDER BY tmp_contact.id DESC LIMIT 10000;

    my $query = <<SQL;
        UPDATE tmp_contact JOIN adresse ON tmp_contact.hexacle = adresse.adresse_src_id SET tmp_contact.id_adr_free = adresse.adresse_id, tmp_contact.voie_id = adresse.voie_id, tmp_contact.operateur = adresse.operateur, tmp_contact.date_eligibilite_ftth = DATE_SUB(adresse.date_mes_commercial, INTERVAL 5 DAY), tmp_contact.nro = adresse.code_nro;
SQL

    return $query;
}

sub tmp_update_contact_from_resil {
    my ($this) = @_;

# INSIDE THE SCRIPT TO INSERT
# UPDATE tmp_contact as c INNER JOIN resil ON c.id_adr_free = resil.id_adr_free SET c.state = 7, c.adr2 = resil.adr2, c.date_de_creation_contrat_mobile = date_format(resil.date_de_creation_contrat_mobile, '%Y-%m-%d') WHERE c.nom = resil.nom AND c.prenom = resil.prenom AND c.state != 7 ORDER BY c.id DESC LIMIT 10000;

    # UPDATE  IF WE KEEP THE RESIL CONTACT
    my $query = <<SQL;
    UPDATE tmp_contact INNER JOIN resil ON tmp_contact.id_adr_free = resil.id_adr_free SET tmp_contact.state = 7, tmp_contact.adr2 = resil.adr2, tmp_contact.date_de_creation_contrat_mobile = date_format(resil.date_de_creation_contrat_mobile, '%Y-%m-%d') ORDER BY tmp_contact.id DESC LIMIT 10000;
SQL

    return $query;
}

sub tmp_update_all_contact_from_resil {
    my ($this) = @_;

    my $query = <<SQL;
    UPDATE tmp_contact INNER JOIN resil ON tmp_contact.id_adr_free = resil.id_adr_free SET tmp_contact.state = 7, tmp_contact.adr2 = resil.adr2, tmp_contact.date_de_creation_contrat_mobile = date_format(resil.date_de_creation_contrat_mobile, '%Y-%m-%d');
SQL

    return $query;
}


#############
#           #
#  INSERT   #
#           #
#############

sub insert_new_file {
    my ($this) = @_;

    my $query = <<SQL;
        INSERT INTO file SET name = ?, nbr = ?;
SQL

    return $query;

}

sub insert_new_file_feedback {
    my ($this) = @_;

    my $query = <<SQL;
        INSERT INTO file_feedback SET name = ?, nbr = ?;
SQL

    return $query;

}

sub insert_table_bookers {
    my ($this) = @_;

    my $columns
        = "id_contact_src, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, optin_appel, optin_sms, optin_courrier, optin_email, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd";
    my $placeholders = join( ",", ("?") x 38 );
    my $query
        = "INSERT IGNORE INTO contact ($columns) VALUES ($placeholders)";

    return $query;
}

sub insert_table_resil {
    my ($this) = @_;
    my $columns
        = "id_contact_src, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, optin_appel, optin_sms, optin_courrier, optin_email, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email";
    my $placeholders = join( ",", ("?") x 37 );
    my $query        = "INSERT IGNORE INTO resil ($columns) VALUES ($placeholders)";

    return $query;
}

sub insert_table_elig {
    my ($this) = @_;

    my $query = <<SQL;
        INSERT INTO tmp_ref_eligibilite SET 
            id_adr_free = ?,
            adresse_num_adresse_suffixe = ?,
            voie_code_fantoir = ?,
            voie_code_insee = ?,
            voie = ?,
            gestionnaire_nom = ?,
            operateur = ?,
            zone = ?,
            pm_nom = ?,
            techno = ?,
            nro = ?,
            date_elig = ?,
            date_j3m = ?
SQL

    return $query;
}

sub insert_table_ref_eli {
    my ($this) = @_;

    my $query = <<SQL;
    INSERT INTO ref_eligibilite (ID_ADR_FREE, ADRESSE_NUM_ADRESSE_SUFFIXE, VOIE_CODE_FANTOIR, VOIE_CODE_INSEE, VOIE, GESTIONNAIRE_NOM, OPERATEUR, ZONE, TECHNO, PM_NOM, NRO, DATE_ELIG, DATE_J3M, ANCIENNETE_ELI_FTTH, NOMBRE_LOGEMENTS, DATE_MISE_A_JOUR) SELECT ID_ADR_FREE, ADRESSE_NUM_ADRESSE_SUFFIXE, VOIE_CODE_FANTOIR, VOIE_CODE_INSEE, VOIE, GESTIONNAIRE_NOM, OPERATEUR, ZONE, TECHNO, PM_NOM, NRO, DATE_ELIG, DATE_J3M, ANCIENNETE_ELI_FTTH, NOMBRE_LOGEMENTS, DATE_MISE_A_JOUR FROM load_ref_eligibilite a WHERE seqnum=1 AND not exists (select null from ref_eligibilite b where a.id_adr_free=b.id_adr_free);
SQL

    return $query;
}

sub insert_table_feedback {
    my ($this) = @_;
    my $columns
        = "id_contact_src, code_campagne, civ, nom, prenom, cp, ville, adr1, adr2, adresse_id, date_eligibilite_ftth, nro, hexacle, libelle_offre_mobile, date_de_creation_contrat_mobile, tel1, tel2, email, anciennete_eli_ftth, operateur_infrastructure, type_de_zone, date_de_dernier_appel, statut_dernier_appel, operateur_mobile, date_de_fin_engagement_mobile, operateur_box, date_de_fin_engagement_box, prix_box, techno, id_rgpd, transmit_dispatch, tag_import, agent, direction_contact, type_contact, qualif_lvl1, qualif_lvl2, qualif_lvl3, techno_detenue, abouti, date_appel, optin_freebox, optin_partenaire";
    my $placeholders = join( ",", ("?") x 43 );
    my $query
        = "INSERT INTO contact_feedback ($columns) VALUES ($placeholders)";

    return $query;
}

sub insert_contact_to_contact_histo {
    my ($this) = @_;
    my $columns
        = "id_contact_src, id_file, flag, src, type_src, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, tel1, tel2, hexacle, email, optin_appel, optin_sms, optin_courrier, optin_email, operateur";

    my $columnsJoin
        = "c.id_contact_src, c.id_file, c.flag, c.src, c.type_src, c.civ, c.nom, c.prenom, c.adr1, c.adr2, c.adr3, c.adr4, c.id_adr_free, c.cp, c.ville, c.tel1, c.tel2, c.hexacle, c.email, c.optin_appel, c.optin_sms, c.optin_courrier, c.optin_email, c.operateur";

    my $query
        = "INSERT INTO contact_histo ($columns) SELECT $columnsJoin FROM contact as c JOIN tmp_contact as tc ON c.id_adr_free = tc.id_adr_free WHERE tc.nom = c.nom AND tc.prenom = c.prenom AND tc.adr3 = c.adr3";

    return $query;
}

sub insert_tmp_contact_to_contact {
    my ($this) = @_;
    my $columns
        = "id_contact_src, id_file, flag, state, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, top_client, date_top_client, orig_top_client, optin_appel, optin_sms, optin_courrier, optin_email, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd, techno";
    my $query
        = "INSERT IGNORE INTO contact ($columns) SELECT $columns FROM tmp_contact";

    return $query;
}

sub insert_tmp_contact_to_contact_feedback {
    my ($this) = @_;
    my $columns
        = "id_contact_src, id_file, flag, code_campagne, civ, nom, prenom, cp, ville, adr1, adr2, adresse_id, date_eligibilite_ftth, nro, hexacle, libelle_offre_mobile, date_de_creation_contrat_mobile, tel1, tel2, email, anciennete_eli_ftth, operateur_infrastructure, type_de_zone, date_de_dernier_appel, statut_dernier_appel, operateur_mobile, date_de_fin_engagement_mobile, operateur_box, date_de_fin_engagement_box, prix_box, techno, id_rgpd, transmit_dispatch, tag_import, agent, direction_contact, type_contact, qualif_lvl1, qualif_lvl2, qualif_lvl3, techno_detenue, abouti, date_appel, optin_freebox, optin_partenaire";
    my $query
        = "INSERT INTO contact_feedback ($columns) SELECT $columns FROM tmp_contact_feedback";

    return $query;
}

sub insert_contact_to_tmp_resil {
    my ($this) = @_;

    my $columnsResil
        = "id_contact_src, id_file, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, optin_appel, optin_sms, optin_courrier, optin_email, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd, techno";
    my $columnsContact
        = "c.id_contact_src, c.id_file, c.src, c.type_src, c.code_campagne, c.civ, c.nom, c.prenom, c.adr1, c.adr2, c.adr3, c.adr4, c.id_adr_free, c.cp, c.ville, c.pays, c.tel1, c.tel2, c.code_insee, c.hexacle, c.date_eligibilite_ftth, c.nro, c.libelle_offre_mobile, c.date_de_creation_contrat_mobile, c.anciennete_eli_ftth, c.email, c.optin_appel, c.optin_sms, c.optin_courrier, c.optin_email, c.dt_deb_droit_adr, c.dt_fin_droit_adr, c.dt_deb_droit_tel1, c.dt_fin_droit_tel1, c.dt_deb_droit_tel2, c.dt_fin_droit_tel2, c.dt_deb_droit_email, c.dt_fin_droit_email, c.id_rgpd, c.techno";
    my $query
        = "INSERT INTO tmp_resil ($columnsResil) SELECT DISTINCT $columnsContact FROM contact as c INNER JOIN resil as r ON c.id_adr_free = r.id_adr_free";

    return $query;
}

sub insert_tmp_contact_to_tmp_resil {
    my ($this) = @_;

    my $columnsResil
        = "id_contact_src, id_file, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, optin_appel, optin_sms, optin_courrier, optin_email, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd, techno";
    my $columnsContact
        = "c.id_contact_src, c.id_file, c.src, c.type_src, c.code_campagne, c.civ, c.nom, c.prenom, c.adr1, r.adr2, c.adr3, c.adr4, c.id_adr_free, c.cp, c.ville, c.pays, c.tel1, c.tel2, c.code_insee, c.hexacle, c.date_eligibilite_ftth, c.nro, c.libelle_offre_mobile, date_format(r.date_de_creation_contrat_mobile, '%Y-%m-%d'), c.anciennete_eli_ftth, c.email, c.optin_appel, c.optin_sms, c.optin_courrier, c.optin_email, c.dt_deb_droit_adr, c.dt_fin_droit_adr, c.dt_deb_droit_tel1, c.dt_fin_droit_tel1, c.dt_deb_droit_tel2, c.dt_fin_droit_tel2, c.dt_deb_droit_email, c.dt_fin_droit_email, c.id_rgpd, c.techno";
    my $query
        = "INSERT INTO tmp_resil ($columnsResil) SELECT DISTINCT $columnsContact FROM tmp_contact as c INNER JOIN resil as r ON c.id_adr_free = r.id_adr_free";

    return $query;
}

sub tmp_insert_table_feedback {
    my ($this) = @_;

    my $columns
        = "id_contact_src, code_campagne, civ, nom, prenom, cp, ville, adr1, adr2, adresse_id, date_eligibilite_ftth, nro, hexacle, libelle_offre_mobile, date_de_creation_contrat_mobile, tel1, tel2, email, anciennete_eli_ftth, operateur_infrastructure, type_de_zone, date_de_dernier_appel, statut_dernier_appel, operateur_mobile, date_de_fin_engagement_mobile, operateur_box, date_de_fin_engagement_box, prix_box, techno, id_rgpd, transmit_dispatch, tag_import, agent, direction_contact, type_contact, qualif_lvl1, qualif_lvl2, qualif_lvl3, techno_detenue, abouti, date_appel, optin_freebox, optin_partenaire";
    my $placeholders = join( ",", ("?") x 43 );
    my $query
        = "INSERT INTO tmp_contact_feedback ($columns) VALUES ($placeholders)";
    return $query;
}

sub tmp_insert_table_bookers {
    my ($this) = @_;

    my $columns
        = "id_contact_src, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, optin_appel, optin_sms, optin_courrier, optin_email, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd, techno";
    my $placeholders = join( ",", ("?") x 39 );
    my $query
        = "INSERT IGNORE INTO tmp_contact ($columns) VALUES ($placeholders)";
    return $query;
}

##############################
#                            #
#  TRUNCATE / DROP / DELETE  #
#                            #
##############################

sub delete_contact_exist_in_resil {
    my ($this) = @_;

    my $query = <<SQL;
    DELETE contact FROM contact INNER JOIN resil ON contact.id_adr_free = resil.id_adr_free;
SQL

    return $query;
}


sub delete_tmp_contact_exist_in_main_table {
    my ($this) = @_;
    
        # DELETE c FROM contact as c JOIN tmp_contact as tc ON c.id_adr_free = tc.id_adr_free WHERE c.nom = tc.nom AND c.prenom = tc.prenom AND c.adr3 = tc.adr3;
    my $query = <<SQL;
        DELETE tc FROM tmp_contact as tc JOIN contact as c ON tc.id_adr_free = c.id_adr_free WHERE tc.nom = c.nom AND tc.prenom = c.prenom AND tc.adr3 = c.adr3;
SQL

    return $query;
}

sub truncate_table_resil {
    my ($this) = @_;

    my $query = <<SQL;
    TRUNCATE resil;
SQL

    return $query;
}

sub truncate_table_tmp_resil {
    my ($this) = @_;

    my $query = <<SQL;
    TRUNCATE tmp_resil;
SQL

    return $query;
}

sub truncate_table_eli {
    my ($this) = @_;

    my $query = <<SQL;
    TRUNCATE ref_eligibilite;
SQL

    return $query;
}

sub truncate_table_tmp_eli {
    my ($this) = @_;

    my $query = <<SQL;
    TRUNCATE tmp_ref_eligibilite;
SQL

    return $query;
}

sub drop_table_eli {
    my ($this) = @_;

    my $query = <<SQL;
    DROP TABLE IF EXISTS load_ref_eligibilite;
SQL

    return $query;
}

sub create_table_eli {
    my ($this) = @_;

    my $query = <<SQL;
    CREATE TABLE load_ref_eligibilite as select *,row_number() over (partition by id_adr_free order by date_mise_a_jour ) as seqnum from tmp_ref_eligibilite;
SQL

    return $query;
}


sub tmp_truncate_tmp_contact {
    my ($this) = @_;

    my $query = <<SQL;
        TRUNCATE tmp_contact;
SQL

    return $query;
}

sub tmp_truncate_tmp_contact_feedback {
    my ($this) = @_;

    my $query = <<SQL;
        TRUNCATE tmp_contact_feedback;
SQL

    return $query;
}


########################################################
#
# Pour les requetes temporaires !
#
########################################################

sub tmp_select_all_data_fresh_date_elig {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT id_contact_src, src, type_src, code_campagne, civ, nom, prenom, adr1, adr2, adr3, adr4, id_adr_free, cp, ville, pays, tel1, tel2, code_insee, hexacle, date_eligibilite_ftth, nro, libelle_offre_mobile, date_de_creation_contrat_mobile, anciennete_eli_ftth, email, top_client, date_top_client, orig_top_client, optin_appel, optin_sms, optin_courrier, optin_email, operateur, dt_deb_droit_adr, dt_fin_droit_adr, dt_deb_droit_tel1, dt_fin_droit_tel1, dt_deb_droit_tel2, dt_fin_droit_tel2, dt_deb_droit_email, dt_fin_droit_email, id_rgpd, techno FROM tmp_contact WHERE date_eligibilite_ftth >= DATE_SUB(now(), INTERVAL 10 DAY) AND orig_top_client IS NULL
SQL

    return $query;
}

sub tmp_select_topage_client_unset_all {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT id_contact_src, nom, prenom, tel1, tel2, id_adr_free, cp, code_insee, email FROM tmp_contact WHERE orig_top_client IS NULL;
SQL

    return $query;
}

sub tmp_select_topage_client_unset {
    my ($this) = @_;

        # SELECT id_contact_src, nom, prenom, tel1, tel2, id_adr_free, cp, code_insee, email FROM tmp_contact WHERE orig_top_client IS NULL ORDER BY id DESC LIMIT 10000;
    my $query = <<SQL;
        SELECT id_contact_src, nom, prenom, tel1, tel2, id_adr_free, cp, code_insee, email FROM tmp_contact WHERE orig_top_client IS NULL;
SQL

    return $query;
}

sub tmp_select_flag_feedback_unset {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT id_contact_src, qualif_lvl1, qualif_lvl2 FROM tmp_contact_feedback ORDER BY id DESC LIMIT 10000;
SQL

    return $query;
}

sub tmp_select_last_row {
    my ($this) = @_;

    my $query = <<SQL;
        SELECT id FROM tmp_contact ORDER BY id DESC lIMIT 1;
SQL

    return $query;
}



1;
