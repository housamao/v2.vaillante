package vtl_file;

use lib './';

use strict;
use utf8;
use Encode qw(encode_utf8 decode);
use JSON;
use Data::Dumper;
use Text::CSV_XS;
use File::Copy;
use Term::ProgressBar;
use Try::Tiny;
use Net::FTP;
use Net::SFTP;
use Net::SMTP;
use Net::FTPSSL;
use MIME::Lite;
use Fcntl qw(:flock);
use Date::Simple ( 'date', 'today' );

use db;

my $resilFile;
my $zedate             = today() - 60;
my $years              = $zedate - 365 * 3;
my $today              = today->year . "-" . today->month . "-" . today->day;
my $dbh                = vtl_db->get_db();
my $dbh_free           = vtl_db->get_db_free();
my $insertData         = vtl_db->insert_table_bookers();
my $insertDataElig     = vtl_db->insert_table_elig();
my $insertDataFeedback = vtl_db->insert_table_feedback();
my $insertDataResil    = vtl_db->insert_table_resil();
my $pathRefElig        = "/home/hossen/ref_eligibilite/";
my $destinationArchiveFiles = "/home/hossen/OLD/";

sub new {

    my ($class) = @_;
    my $this = {};

    bless( $this, $class );
    return $this;

}

sub exportGroup {
    my ($this) = @_;

    my $file = $this->exportDataGroupByCP();

    print "[SEND] Send email \n";
    $this->sendEmailWithLinkSMTP( $file->{filename} );
    print "|-----> DONE \n";
}

sub exportFreshData {
    my ($this) = @_;

    my $file = $this->exportDataFreshDateElig();
    print "[SEND] Send email \n";
    $this->sendEmailWithLinkSMTP( $file->{filename} );
    print "|-----> DONE \n";
}

sub exportFreshDataFromEligTable {
    my ($this) = @_;

    my $file = $this->exportDataFreshDateEligFromTable();
    print "[SEND] Send email \n";
    $this->sendEmailWithLinkSMTP( $file->{filename} );
    print "|-----> DONE \n";
}

sub exportDataTestWithCopyToFTP {
    my ($this) = @_;

    my $file = $this->exportTest();
    $this->pushCSVtoSFTP($file->{path}, $file)
}

sub resumeInsert {
    my ( $this, $path, $singleFile ) = @_;

    print
        "[STEP 1] - 1 | Reprise de l'insertion du fichier -> $singleFile \n";
    my $line = $this->getLastIdForResume();

    print
        "[STEP 1] - 1.2 | Creation d'un fichier temporaire a la ligne -> $line->{id} \n";
    my $fileForResume
        = $this->createTmpFileForResume( $path, $singleFile, $line->{id} );
    print "1.2 |-----> DONE \n";

    $this->step1( $path, undef, $fileForResume, 1 );

    # print "[STEP 1] - 1.3 | Insertion des donnees dans la table contact \n";
    # $this->tmp_insertDataSingleFile( $path, $fileForResume );
    # print "1.3 |-----> DONE \n";

    # print "[STEP 1] - 1.3.1 | Mise a jour depuis la table adresse \n";
    # $this->tmp_updateDataFromAdress();
    # print "1.3.1 |-----> DONE \n";

    # print "[STEP 1] - 1.3.2 | Mise a jour depuis la table resil \n";
    # $this->insertToTmpresil();
    # print "1.3.2 |-----> DONE \n";

    # print "[STEP 1] - 1.4 | Copie dans le dossier archive \n";
    # $this->copyFileToArchiveFolder( $path, $singleFile );
    # print "1.4 |-----> DONE \n";

    # print "[STEP 1] - 1.5 | Renomme le fichier \n";
    # $this->renameFile( $path, $singleFile );
    # print "1.5 |-----> DONE \n";

    print "[STEP 1] - 1.6 | Supprime le fichier temporaire \n";
    unlink $fileForResume;
    print "1.6 |-----> DONE \n";

    # print "[STEP 1] - 1.7 | Copie sur la table principale \n";
    # $this->copyTableTmpToMain();
    # print "1.7 |-----> DONE \n";

    # print "1 |-----> DONE \n";
}

sub fileProcessing {
    my ( $this, $path, $file ) = @_;

    print
        "[STEP 1] - 1.4 | Creation du fichier avec les info enrichis et flag  \n";
    my $fileName = $this->createContactFileWithAllInfo($file);
    print "1.4 |-----> DONE \n";

    print "[STEP 1] - 1.5 | Depot du fichier sur le ftp  \n";
    $this->pushCSVtoSFTP($fileName);
    print "1.5 |-----> DONE \n";

    # print
    #     "[STEP 1] - 1.6 | Envoie d'un email pour notifier les personnes que le fichier est pret \n";
    # $this->sendEmailWithLinkSMTP( $fileName->{filename},
    #     "https://vaillante.proxad.net/OUT/" );
    # print "1.6 |-----> DONE \n";

}

sub historyAndCleaning {
    my ($this) = @_;

    my $sth;
    my $truncate    = vtl_db->tmp_truncate_tmp_contact();
    # my $insertHisto = vtl_db->insert_contact_to_contact_histo();
    # my $deleteExistsContact
    #     = vtl_db->delete_tmp_contact_exist_in_main_table();

    # print "[STEP 1] - 1.7 | Historisation en cours \n";
    # $sth = $dbh->prepare($insertHisto);
    # $sth->execute();
    # $sth->finish;
    # print "1.7 |-----> DONE \n";

    # print
    #     "[STEP 1] - 1.8 | Suppression des contacts existant dans la table temporaire\n";
    # $sth = $dbh->prepare($deleteExistsContact);
    # $sth->execute();
    # $sth->finish;
    # print "1.8 |-----> DONE \n";

    print "[STEP 1] - 1.9 | Copie sur la table principale \n";
    $this->copyTableTmpToMain();
    print "1.9 |-----> DONE \n";

    print "[STEP 1] - 2 | Suppression de la table temporaire \n";
    $sth = $dbh->prepare($truncate);
    $sth->execute();
    $sth->finish;
    print "2 |-----> DONE \n";
}

sub step1 {
    my ( $this, $path, $files, $singleFile, $isResume ) = @_;

    my $sth;
    my $truncate    = vtl_db->tmp_truncate_tmp_contact();
    my $insertHisto = vtl_db->insert_contact_to_contact_histo();
    my $deleteExistsContact
        = vtl_db->delete_tmp_contact_exist_in_main_table();
    print "[STEP 1] - 1 | Insertion d'un nouveau fichier \n";
    if ($singleFile) {
        if ( !$isResume ) {
            $this->insertFile( $path, $singleFile );
            print "$singleFile START \n\n";
        }

        print
            "[STEP 1] - 1.1 | Insertion des donnees dans la table contact \n";
        $this->tmp_insertDataSingleFile( $path, $singleFile );
        print "1.1 |-----> DONE \n";

        $this->tmp_updateDataFromAdress();
        $this->tmp_updateContactFromResil();
        $this->tmp_topageClient();

        exit;

        print
            "[STEP 1] - 1.2 | Mise a jour des contacts en rattachant a un id file \n";
        $this->updateDataWithIdFile();
        print "1.2 |-----> DONE \n";

        print "[STEP 1] - 1.3 | Mise a jour de la table file  \n";
        $this->updateFile();
        print "1.3 |-----> DONE \n";

        print "[STEP 1] - 1.4 | Copie dans le dossier archive \n";
        $this->copyFileToArchiveFolder( $path, $singleFile );
        print "1.4 |-----> DONE \n";

        print "[STEP 1] - 1.5 | Renomme le fichier \n";
        $this->renameFile( $path, $singleFile );
        print "1.5 |-----> DONE \n";

        print "[STEP 1] - 1.6 | Historisation en cours \n";
        $sth = $dbh->prepare($insertHisto);
        $sth->execute();
        $sth->finish;
        print "1.6 |-----> DONE \n";

        print
            "[STEP 1] - 1.7 | Suppression des contacts existant dans la table temporaire\n";
        $sth = $dbh->prepare($deleteExistsContact);
        $sth->execute();
        $sth->finish;
        print "1.7 |-----> DONE \n";

        print "[STEP 1] - 1.8 | Copie sur la table principale \n";
        $this->copyTableTmpToMain();
        print "1.8 |-----> DONE \n";

        print "[STEP 1] - 1.9 | Suppression de la table temporaire \n";
        $sth = $dbh->prepare($truncate);
        $sth->execute();
        $sth->finish;
        print "1.9 |-----> DONE \n";

        print "$singleFile |-----> DONE \n";
    }
    else {
        for ( my $j = 0; $j <= scalar @$files - 1; $j++ ) {

            print "@$files[$j] START \n\n";

            print
                "[STEP 1] - 1.1 | Insertion des donnees dans la table contact \n";
            $this->tmp_insertDataSingleFile( $path, @$files[$j] );
            print "1.1 |-----> DONE \n";

            print
                "[STEP 1] - 1.2 | Mise a jour des contacts en rattachant a un id file \n";
            $this->updateDataWithIdFile();
            print "1.2 |-----> DONE \n";

            print "[STEP 1] - 1.3 | Mise a jour de la table file  \n";
            $this->updateFile();
            print "1.3 |-----> DONE \n";

            print "[STEP 1] - 1.4 | Copie dans le dossier archive \n";
            $this->copyFileToArchiveFolder( $path, @$files[$j] );
            print "1.4 |-----> DONE \n";

            print "[STEP 1] - 1.5 | Renomme le fichier \n";
            $this->renameFile( $path, @$files[$j] );
            print "1.5 |-----> DONE \n";

            print "[STEP 1] - 1.6 | Historisation en cours \n";
            $sth = $dbh->prepare($insertHisto);
            $sth->execute();
            $sth->finish;
            print "1.6 |-----> DONE \n";

            print
                "[STEP 1] - 1.7 | Suppression des contacts existant dans la table temporaire\n";
            $sth = $dbh->prepare($deleteExistsContact);
            $sth->execute();
            $sth->finish;
            print "1.7 |-----> DONE \n";

            print "[STEP 1] - 1.8 | Copie sur la table principale \n";
            $this->copyTableTmpToMain();
            print "1.8 |-----> DONE \n";

            print "[STEP 1] - 1.9 | Suppression de la table temporaire \n";
            $sth = $dbh->prepare($truncate);
            $sth->execute();
            $sth->finish;
            print "1.9 |-----> DONE \n";

            print "@$files[$j] |-----> DONE \n";
        }
    }
}

sub step1NotOptimize {
    my ( $this, $path, $files, $singleFile ) = @_;
    print "[STEP 1] - 1 | Insertion des donnees dans la table contact \n";
    if ($singleFile) {

        $this->insertDataSingleFile( $path, $singleFile );
        print "[STEP 1] - 1.2 | Copie dans le dossier archive \n";
        $this->copyFileToArchiveFolder( $path, $singleFile );
        print "1.2 |-----> DONE \n";

        print "[STEP 1] - 1.3 | Renomme le fichier \n";
        $this->renameFile( $path, $singleFile );
        print "1.3 |-----> DONE \n";
    }
    else {
        for ( my $j = 0; $j <= scalar @$files - 1; $j++ ) {
            $this->insertDataSingleFile( $path, @$files[$j] );
            print "[STEP 1] - 1.2 | Copie dans le dossier archive \n";
            $this->copyFileToArchiveFolder( $path, @$files[$j] );
            print "1.2 |-----> DONE \n";

            print "[STEP 1] - 1.3 | Renomme le fichier \n";
            $this->renameFile( $path, @$files[$j] );
            print "1.3 |-----> DONE \n";
        }
    }

    print "1 |-----> DONE \n";

}

# Functions related to eligibilite
sub step2 {
    my ( $this, $regFiles ) = @_;
    my $dropTableEli      = vtl_db->drop_table_eli();
    my $createTableEli    = vtl_db->create_table_eli();
    my $insertTableRefEli = vtl_db->insert_table_ref_eli();
    my $truncateEli       = vtl_db->truncate_table_eli();
    my $truncateTmpEli    = vtl_db->truncate_table_tmp_eli();

    print "[STEP 2] - 1 | Nettoyage de la table ref_eligibilite \n";
    my $sth = $dbh->prepare($truncateEli);
    $sth->execute();
    $sth->finish;
    print "1 |-----> DONE \n";

    print "[STEP 2] - 2 | Nettoyage de la table tmp_ref_eligibilite \n";
    $sth = $dbh->prepare($truncateEli);
    $sth->execute();
    $sth->finish;
    print "2 |-----> DONE \n";

    for ( my $j = 0; $j <= scalar @$regFiles - 1; $j++ ) {

        print "[STEP 2] - 3 | Insertion des nouvelles adresses eligible \n";
        $this->insertDataSingleFileElig( $pathRefElig, @$regFiles[$j] );
        print "3 |-----> DONE \n";

        print "[STEP 2] - 4 | Nettoyage de la table temporaire referente \n";
        $sth = $dbh->prepare($dropTableEli);
        $sth->execute();
        $sth->finish;
        print "4 |-----> DONE \n";

        print "[STEP 2] - 5 | Creation de la table \n";
        $sth = $dbh->prepare($createTableEli);
        $sth->execute();
        $sth->finish;
        print "5 |-----> DONE \n";

        print "[STEP 2] - 6 | Insertion des donnees dans la table \n";
        $sth = $dbh->prepare($insertTableRefEli);
        $sth->execute();
        $sth->finish;
        print "6 |-----> DONE \n";

        print "[STEP 2] - 7 | Suppression de la table temporaire \n";
        $sth = $dbh->prepare($dropTableEli);
        $sth->execute();
        $sth->finish;
        print "7 |-----> DONE \n";
    }
}

# Functions related to resiliation
sub step3 {
    my ( $this, $path, $file ) = @_;
    my $truncateTmpResil        = vtl_db->truncate_table_tmp_resil();
    my $insertToTmpResil        = vtl_db->insert_contact_to_tmp_resil();
    my $updateContactTotmpResil = vtl_db->update_contact_from_tmp_resil();

    if ($file) {
        $resilFile = $path . $file;
    }

    print "[STEP 3] - 1 | Nettoyage de la table \n";
    my $truncateResil = vtl_db->truncate_table_resil();
    my $sth           = $dbh->prepare($truncateResil);
    $sth->execute();
    $sth->finish;
    print "1 |-----> DONE \n";

    print "[STEP 3] - 2 | Creation du fichier resil \n";
    $this->createResilProspectsFile($path);
    print "2 |-----> DONE \n";

    print "[STEP 3] - 4 | Insertion des donnees dans la table resil \n";
    $this->insertResilProspects($resilFile);
    print "4 |-----> DONE \n";

    print "[STEP 3] - 5 | Nettoyage de la table temporaire resil \n";
    $sth = $dbh->prepare($truncateTmpResil);
    $sth->execute();
    $sth->finish;
    print "5 |-----> DONE \n";

    print "[STEP 3] - 6 | Déplace les contacts dans une table temporaire \n";
    $sth = $dbh->prepare($insertToTmpResil);
    $sth->execute();
    $sth->finish;
    print "6 |-----> DONE \n";

    print "[STEP 3] - 7 | Mise a jour des contacts qui sont resilie \n";
    $sth = $dbh->prepare($updateContactTotmpResil);
    $sth->execute();
    $sth->finish;
    print "7 |-----> DONE \n";
}

# Functions related to insert feedback from prospect service
sub step4 {
    my ( $this, $path, $files, $singleFile, $isResume ) = @_;
    my $sth;
    my $truncate = vtl_db->tmp_truncate_tmp_contact_feedback();

    print "[STEP 1] - 1 | Insertion d'un nouveau fichier \n";
    if ($singleFile) {
        if ( !$isResume ) {
            $this->insertFile( $path, $singleFile, \1 );
            print "$singleFile START \n\n";
        }

        print
            "[STEP 1] - 1.1 | Insertion des donnees dans la table contact feedback \n";
        $this->tmp_insertDataSingleFileFeedback( $path, $singleFile );
        print "1.1 |-----> DONE \n";

        print "[STEP 1] - 1.2 | Flag les differents status \n";
        $this->tmp_setFlagFeedback();
        print "1.2 |-----> DONE \n";

        print
            "[STEP 1] - 1.3 | Mise a jour des contacts en rattachant a un id file \n";
        $this->updateDataWithIdFile( \1 );
        print "1.3 |-----> DONE \n";

        print "[STEP 1] - 1.4 | Mise a jour de la table file  \n";
        $this->updateFile( \1 );
        print "1.4 |-----> DONE \n";

        print "[STEP 1] - 1.4 | Copie sur la table principale \n";
        $this->copyTableTmpToMain( \1 );
        print "1.4 |-----> DONE \n";

        print "[STEP 1] - 1.5 | Suppression de la table temporaire \n";
        $sth = $dbh->prepare($truncate);
        $sth->execute();
        $sth->finish;
        print "1.5 |-----> DONE \n";

        print "$singleFile |-----> DONE \n";
    }
    else {
        for ( my $j = 0; $j <= scalar @$files - 1; $j++ ) {

            $this->insertFile( $path, @$files[$j], \1 );
            print "@$files[$j] START \n\n";

            print
                "[STEP 1] - 1.1 | Insertion des donnees dans la table contact feedback \n";
            $this->tmp_insertDataSingleFileFeedback( $path, @$files[$j] );
            print "1.1 |-----> DONE \n";

            print "[STEP 1] - 1.2 | Flag les differents status \n";
            $this->tmp_setFlagFeedback();
            print "1.2 |-----> DONE \n";

            print
                "[STEP 1] - 1.3 | Mise a jour des contacts en rattachant a un id file \n";
            $this->updateDataWithIdFile( \1 );
            print "1.3 |-----> DONE \n";

            print "[STEP 1] - 1.4 | Mise a jour de la table file  \n";
            $this->updateFile( \1 );
            print "1.4 |-----> DONE \n";

            print "[STEP 1] - 1.4 | Copie sur la table principale \n";
            $this->copyTableTmpToMain( \1 );
            print "1.4 |-----> DONE \n";

            print "[STEP 1] - 1.5 | Suppression de la table temporaire \n";
            $sth = $dbh->prepare($truncate);
            $sth->execute();
            $sth->finish;
            print "1.5 |-----> DONE \n";

            print "@$files[$j] |-----> DONE \n";
        }
    }
}

sub insertFeedbackContact {
    my ( $this, $path, $file ) = @_;
    my $value;
    $dbh->{'AutoCommit'} = 0;
    if ( $path || $file ) {
        my $errors;
        my $i          = 0;
        my $dataInsert = 0;
        my $nextUpdate = 0;
        my $count      = 0;
        my $batchSize  = 10000;
        my $total      = $this->countLinesCSV( $path . $file );
        my @values;
        my @dataBatch;
        my $sth = $dbh->prepare($insertDataFeedback);
        my $csv = Text::CSV->new( { sep_char => ";", blank_is_undef => 1 } );

        open my $fh, '<:', $path . $file
            or die qq{Unable to open "$path . $file" for input: $!};

        $csv->getline($fh);

        print "\n";    #Si non la progresse bar se superpose

        my $progress_bar = Term::ProgressBar->new(
            (   {   ETA   => 'linear',
                    name  => "[INSERT] ",
                    count => $total
                }
            )
        );

        while ( my $row = $csv->getline($fh) ) {
            chomp $row;
            push @dataBatch, [ @$row[ 0 .. 43 ] ];
        }

        foreach my $enregistrement (@dataBatch) {
            push @values, $enregistrement;
            $count++;
            if (   $count % $batchSize == 0
                || $count == $total )
            {
                try {

                    $dataInsert = $count;

                    $sth->execute(@$_) for @values;
                    @values = ();

                }
                catch {
                    $errors .= "[$i] got dbi error: $_";
                };
            }
            $i++;
            if ( $i % 10000 eq 0 ) {
                $dbh->commit();
            }
            $nextUpdate = $progress_bar->update($i)
                if $i >= $nextUpdate;
        }
        $sth->finish;

        $progress_bar->update($total)
            if $total >= $nextUpdate;

        $dbh->commit();

        print
            "$dataInsert / $i nouvelles lignes sont enregistrees dans la BDD\n";

        if ($errors) {
            $errors .= "\n$dataInsert / $i, donnees insere.";
            $this->createErrorFile( $errors, $file );
        }

        return 0;
    }
    else {
        print "L'un des arguments -path ou -file n'est pas renseigné(s)\n";
    }
    return 0;
}

sub insertFile {
    my ( $this, $path, $file, $isFeedback ) = @_;

    if ($isFeedback) {
        my $total = $this->countLinesCSV( $path . $file, undef, \1 );

        my $insertNewFile = vtl_db->insert_new_file_feedback();
        my $sth           = $dbh->prepare($insertNewFile);

        $sth->execute( $file, $total );
        $sth->finish;
    }
    else {

        my $total = $this->countLinesCSV( $path . $file );

        my $insertNewFile = vtl_db->insert_new_file();
        my $sth           = $dbh->prepare($insertNewFile);

        $sth->execute( $file, $total );
        $sth->finish;
    }

    return 0;

}

sub updateFile {
    my ( $this, $isFeedback ) = @_;

    my $sth;
    my $updateFile;
    if ($isFeedback) {
        $updateFile = vtl_db->update_file_feedback();
        $sth        = $dbh->prepare($updateFile);

        $sth->execute();
        $sth->finish;
    }
    else {
        $updateFile = vtl_db->update_file();
        $sth        = $dbh->prepare($updateFile);

        $sth->execute();
        $sth->finish;
    }
}

sub updateDataWithIdFile {
    my ( $this, $isFeedback ) = @_;

    my $sth;
    my $updateData;
    if ($isFeedback) {

        $updateData = vtl_db->update_data_with_id_file_feedback();
        $sth        = $dbh->prepare($updateData);

        $sth->execute();
        $sth->finish;
    }
    else {
        $updateData = vtl_db->update_data_with_id_file();
        $sth        = $dbh->prepare($updateData);

        $sth->execute();
        $sth->finish;
    }

}

sub insertDataNative {
    my ( $this, $path, $file ) = @_;
    my $value;
    $dbh->{'AutoCommit'} = 0;
    if ( $path || $file ) {
        my $errors;
        my $i          = 0;
        my $dataInsert = 0;
        my $nextUpdate = 0;
        my $count      = 0;
        my $batchSize  = 10000;
        my $total      = $this->countLinesCSV( $path . $file, \1 );
        my @values;
        my @dataBatch;
        my $sth = $dbh->prepare($insertData);
        my $csv = Text::CSV->new( { sep_char => "|", blank_is_undef => 1 } );

        open my $fh, '<', $path . $file
            or die qq{Unable to open "$path . $file" for input: $!};

        print "\n";    #Si non la progresse bar se superpose

        my $progress_bar = Term::ProgressBar->new(
            (   {   ETA   => 'linear',
                    name  => "[INSERT] ",
                    count => $total
                }
            )
        );

        while ( my $row = <$fh> ) {
            chomp $row;
            push @dataBatch, [ @$row[ 0 .. 37 ] ];
        }

        foreach my $enregistrement (@dataBatch) {
            push @values, $enregistrement;
            $count++;
            if (   $count % $batchSize == 0
                || $count == $total )
            {
                try {

                    $dataInsert = $count;

                    $sth->execute(@$_) for @values;
                    @values = ();

                }
                catch {
                    $errors .= "[$i] got dbi error: $_";
                };
            }
            $i++;
            if ( $i % 10000 eq 0 ) {
                $dbh->commit();
                $this->updateDataFromAdress();
            }
            $nextUpdate = $progress_bar->update($i)
                if $i >= $nextUpdate;
        }
        $sth->finish;

        $progress_bar->update($total)
            if $total >= $nextUpdate;

        $dbh->commit();
        $this->updateDataFromAdress();

        print
            "$dataInsert / $i nouvelles lignes sont enregistrees dans la BDD\n";

        if ($errors) {
            $errors .= "\n$dataInsert / $i, donnees insere.";
            $this->createErrorFile( $errors, $file );
        }

        return 0;
    }
    else {
        print "L'un des arguments -path ou -file n'est pas renseigné(s)\n";
    }
    return 0;
}

sub insertDataSingleFile {
    my ( $this, $path, $file ) = @_;
    my $value;
    $dbh->{'AutoCommit'} = 0;
    if ( $path || $file ) {
        my $errors;
        my $i          = 0;
        my $dataInsert = 0;
        my $nextUpdate = 0;
        my $count      = 0;
        my $batchSize  = 10000;
        my $total      = $this->countLinesCSV( $path . $file );
        my @values;
        my @dataBatch;
        my $sth = $dbh->prepare($insertData);
        my $csv = Text::CSV->new( { sep_char => "|", blank_is_undef => 1 } );

        open my $fh, '<:', $path . $file
            or die qq{Unable to open "$path . $file" for input: $!};

        $csv->getline($fh);

        print "\n";    #Si non la progresse bar se superpose

        my $progress_bar = Term::ProgressBar->new(
            (   {   ETA   => 'linear',
                    name  => "[INSERT] ",
                    count => $total
                }
            )
        );

        while ( my $row = $csv->getline($fh) ) {
            chomp $row;
            push @dataBatch, [ @$row[ 0 .. 37 ] ];
        }

        foreach my $enregistrement (@dataBatch) {
            push @values, $enregistrement;
            $count++;
            if (   $count % $batchSize == 0
                || $count == $total )
            {
                try {

                    $dataInsert = $count;

                    $sth->execute(@$_) for @values;
                    @values = ();

                }
                catch {
                    $errors .= "[$i] got dbi error: $_";
                };
            }
            $i++;
            if ( $i % 10000 eq 0 ) {
                $dbh->commit();
                $this->updateDataFromAdress();

                # $this->updateContactResil();
            }
            $nextUpdate = $progress_bar->update($i)
                if $i >= $nextUpdate;
        }
        $sth->finish;

        $progress_bar->update($total)
            if $total >= $nextUpdate;

        $dbh->commit();
        $this->updateDataFromAdress();

        # $this->updateContactResil();

        print
            "$dataInsert / $i nouvelles lignes sont enregistrees dans la BDD\n";

        if ($errors) {
            $errors .= "\n$dataInsert / $i, donnees insere.";
            $this->createErrorFile( $errors, $file );
        }

        return 0;
    }
    else {
        print "L'un des arguments -path ou -file n'est pas renseigné(s)\n";
    }
    return 0;
}

sub insertDataSingleFileElig {
    my ( $this, $path, $file ) = @_;
    my $value;
    $dbh->{'AutoCommit'} = 0;
    if ( $path || $file ) {
        my $row;
        my $errors;
        my $i          = 0;
        my $dataInsert = 0;
        my $nextUpdate = 0;
        my $count      = 0;
        my $batchSize  = 10000;
        my $total      = $this->countLinesCSV( $path . $file, undef, 1 );
        my @values;
        my @dataBatch;
        my $values_str;

        my $sth = $dbh->prepare($insertDataElig);

        my $csv = Text::CSV->new( { sep_char => "|", blank_is_undef => 1 } );

        open my $fh, '<:encoding(UTF-8)', $path . $file
            or die qq{Unable to open "$path . $file" for input: $!};

        $csv->getline($fh);

        print "\n";    #Si non la progresse bar se superpose
        my $progress_bar = Term::ProgressBar->new(
            (   {   ETA   => 'linear',
                    name  => "[INSERT] ",
                    count => $total
                }
            )
        );

        while ( my $row = $csv->getline($fh) ) {
            chomp $row;
            push @dataBatch, [ @$row[ 0 .. 12 ] ];
        }
        foreach my $enregistrement (@dataBatch) {
            push @values, $enregistrement;
            $count++;
            if (   $count % $batchSize == 0
                || $count == $total )
            {
                my $values_str = join( ", ", @values );
                try {

                    $sth->execute(@$_) for @values;
                    @values = ();

                    $dataInsert = $count;

                }
                catch {
                    $errors .= "[$i] got dbi error: vaniquertamere $_";
                };
            }
            $i++;
            if ( $i % 100000 eq 0 ) {
                $dbh->commit();
            }
            $nextUpdate = $progress_bar->update($i)
                if $i >= $nextUpdate;
        }

        $sth->finish;

        $progress_bar->update($total)
            if $total >= $nextUpdate;
        $dbh->commit();

        print
            "$dataInsert / $i nouvelles lignes sont enregistrees dans la BDD\n";

        if ($errors) {
            $errors .= "\n$dataInsert / $i, donnees insere.";
            $this->createErrorFile( $errors, $file );
        }

        return 0;
    }
    else {
        print "L'un des arguments -path ou -file n'est pas renseigné(s)\n";
    }
    return 0;
}

sub insertResilProspects {
    my ( $this, $file ) = @_;
    if ($file) {
        $dbh->{'AutoCommit'} = 0;
        my $fileName;
        my $errors;
        my @values;
        my @dataBatch;
        my $i          = 0;
        my $dataInsert = 0;
        my $nextUpdate = 0;
        my $count      = 0;
        my $batchSize  = 10000;
        my $total      = $this->countLinesCSV( $file, undef, 1 );
        my $sth        = $dbh->prepare($insertDataResil);

        my $csv = Text::CSV->new(
            {   sep_char       => "|",
                blank_is_undef => 1
            }
        );

        open my $fh, '<:encoding(UTF-8)', $file
            or die qq{Unable to open "$file" for input: $!};

        $csv->getline($fh);

        print "\n";    #Si non la progresse bar se superpose

        my $progress_bar = Term::ProgressBar->new(
            (   {   ETA   => 'linear',
                    name  => "[INSERT] ",
                    count => $total
                }
            )
        );

        while ( my $row = $csv->getline($fh) ) {
            chomp $row;
            push @dataBatch, [ @$row[ 0 .. 36 ] ];
        }
        foreach my $enregistrement (@dataBatch) {
            push @values, $enregistrement;
            $count++;
            if (   $count % $batchSize == 0
                || $count == $total )
            {
                try {

                    $dataInsert = $count;
                    $sth->execute(@$_) for @values;
                    @values = ();

                }
                catch {
                    $errors .= "[$i] got dbi error: $_";
                };
            }
            $i++;
            if ( $i % 10000 eq 0 ) {
                $dbh->commit();
            }
            $nextUpdate = $progress_bar->update($i)
                if $i >= $nextUpdate;
        }
        $sth->finish;

        $progress_bar->update($total)
            if $total >= $nextUpdate;

        $dbh->commit();

        $fileName = $file =~ s/(?:.(?!\/))+$//g;
        $fileName =~ s/^.//gm;
        print
            "$dataInsert / $i nouvelles lignes sont enregistrees dans la BDD\n";

        if ($errors) {
            $errors .= "\n$dataInsert / $i, donnees insere.";
            $this->createErrorFile( $errors, $file );
        }

        return 0;
    }
    else {
        print
            "L'argument -file n'est pas renseigné ou le fichier est soit vide, soit pas crée\n";
    }
    return 0;
}

sub createErrorFile {
    my ( $this, $error, $file ) = @_;

    my $count;
    my $dateD8 = today();

    my $path = "/home/hossen/errors_files/";

    if ( !-d $path ) {
        mkdir( $path, 0755 );
    }

    opendir my $DIR, $path or die "Error in opening dir '$path' : $!";
    $count = grep -f "$path/$_", readdir $DIR;
    closedir $DIR;

    $count++;

    my $nameFile = $count . "- ERR_$file" . "_" . $dateD8->as_d8 . ".log";

    if ( -f $path . $nameFile ) {
        die "ALREADY $nameFile\n";
    }
    open( OUT, ">", $path . $nameFile ) || die "$!";

    print OUT $error;

    close OUT;

}

sub countLinesCSV {
    my ( $this, $file, $isAnotherFile, $isUTF8 ) = @_;
    my $lines = 0;
    my $fh;

    if ($isUTF8) {
        open $fh, '<:encoding(UTF-8)', $file
            or die qq{Unable to open "$file" for input: $!};
    }
    else {
        open $fh, '<', $file
            or die qq{Unable to open "$file" for input: $!};
    }

    if ($isAnotherFile) {
        $lines++ while <$fh>;
    }
    else {
        my $csv = Text::CSV->new( { sep_char => "|", blank_is_undef => 1 } );

        $csv->getline($fh);
        $lines++ while $csv->getline($fh);
    }
    return $lines;
}

sub createResilProspectsFile {
    my ( $this, $path ) = @_;

    my $dateD8 = today();
    my ($id_client, $date_saisie, $prenom_abonne, $nom_abonne,
        $cp,        $date_resil,  $type_offre,    $tel,
        $email,     $societe,     $adresse,       $voie,
        $ville,     $code_insee,  $tel_portable,  $id_dem,
        $id_new,    $id_adresse,  $substh,        $liste_email
    );
    my $count      = 0;
    my $nextUpdate = 0;

    my $campagne = "RESF_0000001_" . $dateD8->as_d8;
    if ( -f $path . $campagne ) {
        die "ALREADY $campagne\n";
    }
    open( OUT, ">:encoding(UTF-8)", $path . $campagne ) || die "$!";

    print OUT
        "ID_CONTACT_SRC|SRC|TYPE_SRC|CODE_CAMPAGNE|CIV|NOM|PRENOM|ADR1|ADR2|ADR3|ADR4|ID_ADR_FREE|CP|VILLE|PAYS|TEL1|TEL2|CODE_INSEE|HEXACLE|DATE_ELIGIBILITEFTTH|NRO|LIBELLE_OFFRE_MOBILE|DATE_DE_CREATION_CONTRAT_MOBILE|ANCIENNETE_ELI_FTTH|EMAIL|OPTIN_APPEL|OPTIN_SMS|OPTIN_COURRIER|OPTIN_EMAIL|DT_DEB_DROIT_ADR|DT_FIN_DROIT_ADR|DT_DEB_DROIT_TEL1|DT_FIN_DROIT_TEL1|DT_DEB_DROIT_TEL2|DT_FIN_DROIT_TEL2|DT_DEB_DROIT_EMAIL|DT_FIN_DROIT_EMAIL\n";

    print "$zedate / $years\n";

    my $sth
        = $dbh_free->prepare(
        "SELECT id_client, DATE_FORMAT(date_saisie,'%Y%m%d') as date_s, prenom_abonne, nom_abonne, code_postal, DATE_FORMAT(date_resil,'%Y%m%d') AS date_f, type_offre, tel, email, societe, adresse, voie, ville, code_insee, tel_portable FROM resil WHERE date_resil<'$zedate' AND date_resil>'$years' and id_presta=1 and type_offre in (1,3,5,20);"
        );
    $sth->execute();
    $sth->bind_columns(
        \(  $id_client, $date_saisie, $prenom_abonne, $nom_abonne,
            $cp,        $date_resil,  $type_offre,    $tel,
            $email,     $societe,     $adresse,       $voie,
            $ville,     $code_insee,  $tel_portable
        )
    );

    my $total        = $sth->rows;
    my $progress_bar = Term::ProgressBar->new(
        (   {   ETA   => 'linear',
                name  => "[DUMP] ",
                count => $total
            }
        )
    );

    while ( $sth->fetch() ) {

        $id_dem = $id_new = '';
        my $substh = $dbh_free->prepare(
            "SELECT id_new FROM demenagement WHERE id_client='$id_client';");
        $substh->execute();
        $substh->bind_columns( \($id_new) );
        $substh->fetch();
        $substh->finish();
        $id_dem = 'DEM ' . $id_new if ($id_new);

        $id_adresse = '';
        $substh
            = $dbh_free->prepare(
            "SELECT id_adresse FROM adresse_fibre WHERE id_client='$id_client';"
            );
        $substh->execute();
        $substh->bind_columns( \($id_adresse) );
        $substh->fetch();
        $substh->finish();

        $liste_email = '';
        $substh
            = $dbh_free->prepare(
            "SELECT id_client FROM liste_email WHERE id_client='$id_client' AND flag in (10);"
            );
        $substh->execute();
        $substh->bind_columns( \$liste_email );
        $substh->fetch();
        $substh->finish();
        my $no_mail = '1';
        $no_mail = '0' if ( $liste_email eq $id_client );

        if ( $societe =~ /"/ ) {
            $societe =~ s/"/""/g;
            $societe = '"' . $societe . '"';
        }

        if ( $voie =~ /"/ ) {
            $voie =~ s/"/""/g;
            $voie = '"' . $voie . '"';
        }

        if ( $adresse =~ /"/ ) {
            $adresse =~ s/"/""/g;
            $adresse = '"' . $adresse . '"';
        }

        if ( $nom_abonne =~ /\|/ ) {
            $nom_abonne =~ s/\|/,/g;
        }

        if ( $prenom_abonne =~ /\|/ ) {
            $prenom_abonne =~ s/\|/,/g;
        }
        if ( $societe =~ /\|/ ) {
            $societe =~ s/\|/,/g;
        }

        print OUT
            "$id_client|RESF|Interne|||$nom_abonne|$prenom_abonne|$societe|$adresse|$voie||$id_adresse|$cp|$ville|||$tel_portable|$code_insee|||||$date_saisie||$email||||$no_mail||||||||\n";

        $count++;
        $nextUpdate = $progress_bar->update($count)
            if $count >= $nextUpdate;

    }

    $dbh_free->disconnect;

    close OUT;
    $count++;

    rename $path . $campagne, $path . $campagne . "_V1_$count.csv";
    $progress_bar->update($total)
        if $total >= $nextUpdate;

    $resilFile = $path . $campagne . "_V1_$count.csv";

    return;
}

sub createContactFileWithAllInfo {
    my ( $this, $fileName ) = @_;

    $fileName =~ s/V1.*//gm;
    my $file;
    my $count      = 0;
    my $nextUpdate = 0;
    my $date       = Date::Simple->new();
    my $dateD8     = today();
    my $line;
    my $campagne   = $fileName;
    my $pathExport = "/var/www/vaillante.proxad.net/OUT/";

    if ( -f $pathExport . $campagne ) {
        die "ALREADY $campagne\n";
    }
    open( OUT, ">:encoding(UTF-8)", $pathExport . $campagne ) || die "$!";

    my ($id_contact_src,                  $src,
        $type_src,                        $code_campagne,
        $civ,                             $nom,
        $prenom,                          $adr1,
        $adr2,                            $adr3,
        $adr4,                            $id_adr_free,
        $cp,                              $ville,
        $pays,                            $tel1,
        $tel2,                            $code_insee,
        $hexacle,                         $date_eligibilite_ftth,
        $nro,                             $libelle_offre_mobile,
        $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
        $email,                           $top_client,
        $date_top_client,                 $orig_top_client,
        $optin_appel,                     $optin_sms,
        $optin_courrier,                  $optin_email,
        $operateur,                       $dt_deb_droit_adr,
        $dt_fin_droit_adr,                $dt_deb_droit_tel1,
        $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
        $dt_fin_droit_tel2,               $dt_deb_droit_email,
        $dt_fin_droit_email,              $id_rgpd,
        $techno
    ) = "";

    print OUT
        uc(
        "id_contact_src|src|type_src|code_campagne|civ|nom|prenom|adr1|adr2|adr3|adr4|id_adr_free|cp|ville|pays|tel1|tel2|code_insee|hexacle|date_eligibilite_ftth|nro|libelle_offre_mobile|date_de_creation_contrat_mobile|anciennete_eli_ftth|email|optin_appel|optin_sms|optin_courrier|optin_email|operateur|dt_deb_droit_adr|dt_fin_droit_adr|dt_deb_droit_tel1|dt_fin_droit_tel1|dt_deb_droit_tel2|dt_fin_droit_tel2|dt_deb_droit_email|dt_fin_droit_email|id_rgpd|techno"
        ) . "\n";

    my $query = vtl_db->select_all_tmp_contact();

    my $sth = $dbh->prepare($query);

    $sth->execute();
    $sth->bind_columns(
        \(  $id_contact_src,                  $src,
            $type_src,                        $code_campagne,
            $civ,                             $nom,
            $prenom,                          $adr1,
            $adr2,                            $adr3,
            $adr4,                            $id_adr_free,
            $cp,                              $ville,
            $pays,                            $tel1,
            $tel2,                            $code_insee,
            $hexacle,                         $date_eligibilite_ftth,
            $nro,                             $libelle_offre_mobile,
            $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
            $email,                           $top_client,
            $date_top_client,                 $orig_top_client,
            $optin_appel,                     $optin_sms,
            $optin_courrier,                  $optin_email,
            $operateur,                       $dt_deb_droit_adr,
            $dt_fin_droit_adr,                $dt_deb_droit_tel1,
            $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
            $dt_fin_droit_tel2,               $dt_deb_droit_email,
            $dt_fin_droit_email,              $id_rgpd,
            $techno
        )
    );

    my $total        = $sth->rows;
    my $progress_bar = Term::ProgressBar->new(
        (   {   ETA   => 'linear',
                name  => "[DUMP] ",
                count => $total
            }
        )
    );

    while ( $sth->fetch() ) {

        $line
            = "$id_contact_src|$src|$type_src|$code_campagne|$civ|$nom|$prenom|$adr1|$adr2|$adr3|$adr4|$id_adr_free|$cp|$ville|$pays|$tel1|$tel2|$code_insee|$hexacle|$date_eligibilite_ftth|$nro|$libelle_offre_mobile|$date_de_creation_contrat_mobile|$anciennete_eli_ftth|$email|$optin_appel|$optin_sms|$optin_courrier|$optin_email|$operateur|$dt_deb_droit_adr|$dt_fin_droit_adr|$dt_deb_droit_tel1|$dt_fin_droit_tel1|$dt_deb_droit_tel2|$dt_fin_droit_tel2|$dt_deb_droit_email|$dt_fin_droit_email|$id_rgpd|$techno\n";

        print OUT $line;

        $count++;
        $nextUpdate = $progress_bar->update($count)
            if $count >= $nextUpdate;

    }

    close OUT;
    $count++;

    rename $pathExport . $campagne,
        $pathExport . $campagne . "V1_$count.csv";
    $progress_bar->update($total)
        if $total >= $nextUpdate;

    $file = $campagne . "V1_$count.csv";

    $sth->finish;

    return { filename => $file };
}

sub exportTest {
    my ($this) = @_;

    print "[EXPORT] all data group by CP\n";

    my $file;
    my $count      = 0;
    my $nextUpdate = 0;
    my $date       = Date::Simple->new();
    my $dateD8     = today();

    my $campagne   = "EXPORT_INFO_" . $dateD8->as_d8;
    my $pathExport = "/var/www/vaillante.proxad.net/OUT/";

    if ( -f $pathExport . $campagne ) {
        die "ALREADY $campagne\n";
    }
    open( OUT, ">:encoding(UTF-8)", $pathExport . $campagne ) || die "$!";

    my ($id_contact_src,                  $src,
        $type_src,                        $code_campagne,
        $civ,                             $nom,
        $prenom,                          $adr1,
        $adr2,                            $adr3,
        $adr4,                            $id_adr_free,
        $cp,                              $ville,
        $pays,                            $tel1,
        $tel2,                            $code_insee,
        $hexacle,                         $date_eligibilite_ftth,
        $nro,                             $libelle_offre_mobile,
        $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
        $email,                           $top_client,
        $date_top_client,                 $orig_top_client,
        $optin_appel,                     $optin_sms,
        $optin_courrier,                  $optin_email,
        $operateur,                       $dt_deb_droit_adr,
        $dt_fin_droit_adr,                $dt_deb_droit_tel1,
        $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
        $dt_fin_droit_tel2,               $dt_deb_droit_email,
        $dt_fin_droit_email,              $id_rgpd,
        $techno
    ) = "";

    print OUT
        uc(
        "id_contact_src|src|type_src|code_campagne|civ|nom|prenom|adr1|adr2|adr3|adr4|id_adr_free|cp|ville|pays|tel1|tel2|code_insee|hexacle|date_eligibilite_ftth|nro|libelle_offre_mobile|date_de_creation_contrat_mobile|anciennete_eli_ftth|email|optin_appel|optin_sms|optin_courrier|optin_email|operateur|dt_deb_droit_adr|dt_fin_droit_adr|dt_deb_droit_tel1|dt_fin_droit_tel1|dt_deb_droit_tel2|dt_fin_droit_tel2|dt_deb_droit_email|dt_fin_droit_email|id_rgpd|techno"
        ) . "\n";

    my $query = vtl_db->select_test_push_to_ftp();

    my $sth = $dbh->prepare($query);

    $sth->execute();
    $sth->bind_columns(
        \(  $id_contact_src,                  $src,
            $type_src,                        $code_campagne,
            $civ,                             $nom,
            $prenom,                          $adr1,
            $adr2,                            $adr3,
            $adr4,                            $id_adr_free,
            $cp,                              $ville,
            $pays,                            $tel1,
            $tel2,                            $code_insee,
            $hexacle,                         $date_eligibilite_ftth,
            $nro,                             $libelle_offre_mobile,
            $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
            $email,                           $top_client,
            $date_top_client,                 $orig_top_client,
            $optin_appel,                     $optin_sms,
            $optin_courrier,                  $optin_email,
            $operateur,                       $dt_deb_droit_adr,
            $dt_fin_droit_adr,                $dt_deb_droit_tel1,
            $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
            $dt_fin_droit_tel2,               $dt_deb_droit_email,
            $dt_fin_droit_email,              $id_rgpd,
            $techno
        )
    );

    my $total        = $sth->rows;
    my $progress_bar = Term::ProgressBar->new(
        (   {   ETA   => 'linear',
                name  => "[DUMP] ",
                count => $total
            }
        )
    );

    while ( $sth->fetch() ) {

        print OUT
            "$id_contact_src|$src|$type_src|$code_campagne|$civ|$nom|$prenom|$adr1|$adr2|$adr3|$adr4|$id_adr_free|$cp|$ville|$pays|$tel1|$tel2|$code_insee|$hexacle|$date_eligibilite_ftth|$nro|$libelle_offre_mobile|$date_de_creation_contrat_mobile|$anciennete_eli_ftth|$email|$optin_appel|$optin_sms|$optin_courrier|$optin_email|$operateur|$dt_deb_droit_adr|$dt_fin_droit_adr|$dt_deb_droit_tel1|$dt_fin_droit_tel1|$dt_deb_droit_tel2|$dt_fin_droit_tel2|$dt_deb_droit_email|$dt_fin_droit_email|$id_rgpd|$techno\n";

        $count++;
        $nextUpdate = $progress_bar->update($count)
            if $count >= $nextUpdate;

    }

    close OUT;
    $count++;

    rename $pathExport . $campagne,
        $pathExport . $campagne . "_V1_$count.csv";
    $progress_bar->update($total)
        if $total >= $nextUpdate;

    $file = $campagne . "_V1_$count.csv";

    $sth->finish;

    print "-----> DONE \n";

    return { filename => $file, path => $pathExport };

}

sub exportDataGroupByCP {
    my ($this) = @_;

    print "[EXPORT] all data group by CP\n";

    my $file;
    my $count      = 0;
    my $nextUpdate = 0;
    my $date       = Date::Simple->new();
    my $dateD8     = today();

    my $campagne   = "EXPORT_INFO_" . $dateD8->as_d8;
    my $pathExport = "/var/www/vaillante.proxad.net/campaign/";

    if ( -f $pathExport . $campagne ) {
        die "ALREADY $campagne\n";
    }
    open( OUT, ">:encoding(UTF-8)", $pathExport . $campagne ) || die "$!";

    my ( $cp, $nbr_cp, $nbr_optin_appel, $nbr_optin_email, $nbr_optin_sms,
        $nbr_optin_courrier );

    print OUT
        uc(
        "cp|nbr_cp|nbr_optin_appel|nbr_optin_email|nbr_optin_sms|nbr_optin_courrier"
        ) . "\n";

    my $query = vtl_db->select_all_data_group_by_cp();

    my $sth = $dbh->prepare($query);

    $sth->execute();
    $sth->bind_columns(
        \(  $cp,              $nbr_cp,        $nbr_optin_appel,
            $nbr_optin_email, $nbr_optin_sms, $nbr_optin_courrier
        )
    );

    my $total        = $sth->rows;
    my $progress_bar = Term::ProgressBar->new(
        (   {   ETA   => 'linear',
                name  => "[DUMP] ",
                count => $total
            }
        )
    );

    while ( $sth->fetch() ) {

        print OUT
            "$cp|$nbr_cp|$nbr_optin_appel|$nbr_optin_email|$nbr_optin_sms|$nbr_optin_courrier\n";

        $count++;
        $nextUpdate = $progress_bar->update($count)
            if $count >= $nextUpdate;

    }

    close OUT;
    $count++;

    rename $pathExport . $campagne,
        $pathExport . $campagne . "_V1_$count.csv";
    $progress_bar->update($total)
        if $total >= $nextUpdate;

    $file = $campagne . "_V1_$count.csv";

    $sth->finish;

    print "-----> DONE \n";

    return { filename => $file };

}

sub exportDataFreshDateElig {
    my ($this) = @_;

    print "[EXPORT] all data fresh date elig 1 month\n";

    my $file;
    my $count      = 0;
    my $nextUpdate = 0;
    my $date       = Date::Simple->new();
    my $dateD8     = today();
    my $line;
    my $campagne   = "EXPORT_" . $dateD8->as_d8;
    my $pathExport = "/var/www/vaillante.proxad.net/campaign/";

    if ( -f $pathExport . $campagne ) {
        die "ALREADY $campagne\n";
    }
    open( OUT, ">:encoding(UTF-8)", $pathExport . $campagne ) || die "$!";

    my ($id_contact_src,                  $src,
        $type_src,                        $code_campagne,
        $civ,                             $nom,
        $prenom,                          $adr1,
        $adr2,                            $adr3,
        $adr4,                            $id_adr_free,
        $cp,                              $ville,
        $pays,                            $tel1,
        $tel2,                            $code_insee,
        $hexacle,                         $date_eligibilite_ftth,
        $nro,                             $libelle_offre_mobile,
        $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
        $email,                           $top_client,
        $date_top_client,                 $orig_top_client,
        $optin_appel,                     $optin_sms,
        $optin_courrier,                  $optin_email,
        $operateur,                       $dt_deb_droit_adr,
        $dt_fin_droit_adr,                $dt_deb_droit_tel1,
        $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
        $dt_fin_droit_tel2,               $dt_deb_droit_email,
        $dt_fin_droit_email,              $id_rgpd,
        $techno
    ) = "";

    print OUT
        uc(
        "id_contact_src|src|type_src|code_campagne|civ|nom|prenom|adr1|adr2|adr3|adr4|id_adr_free|cp|ville|pays|tel1|tel2|code_insee|hexacle|date_eligibilite_ftth|nro|libelle_offre_mobile|date_de_creation_contrat_mobile|anciennete_eli_ftth|email|optin_appel|optin_sms|optin_courrier|optin_email|operateur|dt_deb_droit_adr|dt_fin_droit_adr|dt_deb_droit_tel1|dt_fin_droit_tel1|dt_deb_droit_tel2|dt_fin_droit_tel2|dt_deb_droit_email|dt_fin_droit_email|id_rgpd|techno"
        ) . "\n";

    my $query = vtl_db->select_all_data_with_date_elig();

    my $sth = $dbh->prepare($query);

    $sth->execute();
    $sth->bind_columns(
        \(  $id_contact_src,                  $src,
            $type_src,                        $code_campagne,
            $civ,                             $nom,
            $prenom,                          $adr1,
            $adr2,                            $adr3,
            $adr4,                            $id_adr_free,
            $cp,                              $ville,
            $pays,                            $tel1,
            $tel2,                            $code_insee,
            $hexacle,                         $date_eligibilite_ftth,
            $nro,                             $libelle_offre_mobile,
            $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
            $email,                           $top_client,
            $date_top_client,                 $orig_top_client,
            $optin_appel,                     $optin_sms,
            $optin_courrier,                  $optin_email,
            $operateur,                       $dt_deb_droit_adr,
            $dt_fin_droit_adr,                $dt_deb_droit_tel1,
            $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
            $dt_fin_droit_tel2,               $dt_deb_droit_email,
            $dt_fin_droit_email,              $id_rgpd,
            $techno
        )
    );

    my $total        = $sth->rows;
    my $progress_bar = Term::ProgressBar->new(
        (   {   ETA   => 'linear',
                name  => "[DUMP] ",
                count => $total
            }
        )
    );

    while ( $sth->fetch() ) {

        $line
            = "$id_contact_src|$src|$type_src|$code_campagne|$civ|$nom|$prenom|$adr1|$adr2|$adr3|$adr4|$id_adr_free|$cp|$ville|$pays|$tel1|$tel2|$code_insee|$hexacle|$date_eligibilite_ftth|$nro|$libelle_offre_mobile|$date_de_creation_contrat_mobile|$anciennete_eli_ftth|$email|$optin_appel|$optin_sms|$optin_courrier|$optin_email|$operateur|$dt_deb_droit_adr|$dt_fin_droit_adr|$dt_deb_droit_tel1|$dt_fin_droit_tel1|$dt_deb_droit_tel2|$dt_fin_droit_tel2|$dt_deb_droit_email|$dt_fin_droit_email|$id_rgpd|$techno\n";

        print OUT $line;

        $count++;
        $nextUpdate = $progress_bar->update($count)
            if $count >= $nextUpdate;

    }

    close OUT;
    $count++;

    rename $pathExport . $campagne,
        $pathExport . $campagne . "_V1_$count.csv";
    $progress_bar->update($total)
        if $total >= $nextUpdate;

    $file = $campagne . "_V1_$count.csv";

    $sth->finish;

    print "-----> DONE \n";

    return { filename => $file };

}

sub exportDataFreshDateEligFromTable {
    my ($this) = @_;

    print "[EXPORT] all data fresh date from week\n";

    my $file;
    my $count      = 0;
    my $nextUpdate = 0;
    my $date       = Date::Simple->new();
    my $dateD8     = today();
    my $line;
    my $campagne   = "EXPORT_WEEK_" . $dateD8->as_d8;
    my $pathExport = "/var/www/vaillante.proxad.net/campaign/";

    if ( -f $pathExport . $campagne ) {
        die "ALREADY $campagne\n";
    }
    open( OUT, ">:encoding(UTF-8)", $pathExport . $campagne ) || die "$!";

    my ($id_contact_src,                  $src,
        $type_src,                        $code_campagne,
        $civ,                             $nom,
        $prenom,                          $adr1,
        $adr2,                            $adr3,
        $adr4,                            $id_adr_free,
        $cp,                              $ville,
        $pays,                            $tel1,
        $tel2,                            $code_insee,
        $hexacle,                         $date_eligibilite_ftth,
        $nro,                             $libelle_offre_mobile,
        $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
        $email,                           $top_client,
        $date_top_client,                 $orig_top_client,
        $optin_appel,                     $optin_sms,
        $optin_courrier,                  $optin_email,
        $operateur,                       $dt_deb_droit_adr,
        $dt_fin_droit_adr,                $dt_deb_droit_tel1,
        $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
        $dt_fin_droit_tel2,               $dt_deb_droit_email,
        $dt_fin_droit_email,              $id_rgpd,
        $techno
    ) = undef;

    print OUT
        uc(
        "id_contact_src|src|type_src|code_campagne|civ|nom|prenom|adr1|adr2|adr3|adr4|id_adr_free|cp|ville|pays|tel1|tel2|code_insee|hexacle|date_eligibilite_ftth|nro|libelle_offre_mobile|date_de_creation_contrat_mobile|anciennete_eli_ftth|email|top_client|date_top_client|orig_top_client|optin_appel|optin_sms|optin_courrier|optin_email|operateur|dt_deb_droit_adr|dt_fin_droit_adr|dt_deb_droit_tel1|dt_fin_droit_tel1|dt_deb_droit_tel2|dt_fin_droit_tel2|dt_deb_droit_email|dt_fin_droit_email|id_rgpd|techno"
        ) . "\n";

    my $query = vtl_db->select_all_data_fresh_date_elig_table();

    my $sth = $dbh->prepare($query);

    $sth->execute();
    $sth->bind_columns(
        \(  $id_contact_src,                  $src,
            $type_src,                        $code_campagne,
            $civ,                             $nom,
            $prenom,                          $adr1,
            $adr2,                            $adr3,
            $adr4,                            $id_adr_free,
            $cp,                              $ville,
            $pays,                            $tel1,
            $tel2,                            $code_insee,
            $hexacle,                         $date_eligibilite_ftth,
            $nro,                             $libelle_offre_mobile,
            $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
            $email,                           $top_client,
            $date_top_client,                 $orig_top_client,
            $optin_appel,                     $optin_sms,
            $optin_courrier,                  $optin_email,
            $operateur,                       $dt_deb_droit_adr,
            $dt_fin_droit_adr,                $dt_deb_droit_tel1,
            $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
            $dt_fin_droit_tel2,               $dt_deb_droit_email,
            $dt_fin_droit_email,              $id_rgpd,
            $techno
        )
    );

    my $total        = $sth->rows;
    my $progress_bar = Term::ProgressBar->new(
        (   {   ETA   => 'linear',
                name  => "[DUMP] ",
                count => $total
            }
        )
    );

    while ( $sth->fetch() ) {

        $line
            = "$id_contact_src|$src|$type_src|$code_campagne|$civ|$nom|$prenom|$adr1|$adr2|$adr3|$adr4|$id_adr_free|$cp|$ville|$pays|$tel1|$tel2|$code_insee|$hexacle|$date_eligibilite_ftth|$nro|$libelle_offre_mobile|$date_de_creation_contrat_mobile|$anciennete_eli_ftth|$email|$top_client|$date_top_client|$orig_top_client|$optin_appel|$optin_sms|$optin_courrier|$optin_email|$operateur|$dt_deb_droit_adr|$dt_fin_droit_adr|$dt_deb_droit_tel1|$dt_fin_droit_tel1|$dt_deb_droit_tel2|$dt_fin_droit_tel2|$dt_deb_droit_email|$dt_fin_droit_email|$id_rgpd|$techno\n";

        print OUT $line;

        $count++;
        $nextUpdate = $progress_bar->update($count)
            if $count >= $nextUpdate;

    }

    close OUT;
    $count++;

    rename $pathExport . $campagne,
        $pathExport . $campagne . "_V1_$count.csv";
    $progress_bar->update($total)
        if $total >= $nextUpdate;

    $file = $campagne . "_V1_$count.csv";

    $sth->finish;

    print "-----> DONE \n";

    return { filename => $file };

}

sub pushCSVtoSFTP {
    my ($this, $path, $file) = @_;

    if(!$path) {
        $path = "/var/www/vaillante.proxad.net/OUT/";
    }

    my $urlFile;
    my $user     = "free-dsi-acqui";
    my $password = "cqDmDvfY4oIAcwHyPFmi";
    my $host     = "ftp.mcra.fr";

    my $ftp      = Net::FTPSSL->new($host, Debug => 1, Encryption => EXP_CRYPT, Timeout => 120, Croak => 1) or die "Cannot connect to'$host':$!";
    $ftp->login( $user, $password )
        or die "Cannot login'$host':" . $ftp->message;


    $ftp->cwd("/OUT");

    $ftp->put( $path . $file->{filename} );
    $urlFile = "https://vaillante.proxad.net/OUT/" . $file->{filename};

    $ftp->quit;
    return $urlFile;
}

sub renameFile {
    my ( $this, $path, $file ) = @_;
    my $error;
    try {
        rename $path . $file, $path . "DONE_" . $file;
    }
    catch {
        $error .= "Got rename error: $_";
        $this->createErrorFile( $error, $file );
    };

    return;
}

sub copyFileToArchiveFolder {
    my ( $this, $path, $file ) = @_;
    my $error;

    if ( !-d $path ) {
        mkdir( $path, 0755 );
    }

    try {
        copy( $path . $file, $destinationArchiveFiles . $file );

    }
    catch {
        $error .= "Got copy error: $_";
        $this->createErrorFile( $error, $file );
    };

    return;
}

sub sendEmailWithLinkSMTP {
    my ( $this, $filename, $url ) = @_;

    my $urlFile;

    if ($url) {
        $urlFile = $url;
    }
    else {
        $urlFile = "https://vaillante.proxad.net/campaign/";
    }

    my $to      = "hossen\@zms.proxad.net";
    my $from    = "Vaillante téléchargement <hossen\@zms.proxad.net>";
    my $subject = "Votre fichier $filename est prêt !";
    my $body
        = "Bonjour,\n\nVotre fichier $filename est disponible en téléchargement ici : $urlFile$filename"
        . ".\n";
    my $smtp_server = "smtp.free.fr";

    my $smtp = Net::SMTP->new( $smtp_server, Timeout => 30 )
        or die "Error connecting to SMTP server: $!\n";

    $smtp->mail($from) or die "Error setting from: $!\n";
    $smtp->to( $to, "kanandout\@corp.free.fr", "alevavasseur\@corp.free.fr",
        "rgillmann\@drafree.fr", "amaitre\@iliad.fr", "jfraichard\@iliad.fr" )
        or die "Error setting to: $!\n";

    $smtp->data()                    or die "Error starting data: $!\n";
    $smtp->datasend("From: $from\n") or die "Error sending from: $!\n";
    $smtp->datasend("To: $to\n")     or die "Error sending to: $!\n";
    $smtp->datasend("Subject: $subject\n")
        or die "Error sending subject: $!\n";
    $smtp->datasend("\n")      or die "Error sending body: $!\n";
    $smtp->datasend("$body\n") or die "Error sending body: $!\n";
    $smtp->dataend()           or die "Error ending data: $!\n";

    $smtp->quit() or die "Error disconnecting from SMTP server: $!\n";

    print
        "Email sent successfully to $to, kanandout\@corp.free.fr, alevavasseur\@corp.free.fr, rgillmann\@drafree.fr $filename\n";
}

sub copyTableTmpToMain {
    my ( $this, $isFeedback ) = @_;

    if ($isFeedback) {
        my $copy = vtl_db->insert_tmp_contact_to_contact_feedback;
        my $sth  = $dbh->prepare($copy);

        $sth->execute();
        $sth->finish;
    }
    else {
        my $copy = vtl_db->insert_tmp_contact_to_contact;
        my $sth  = $dbh->prepare($copy);

        $sth->execute();
        $sth->finish;
    }

    return;
}

sub getLastIdForResume {
    my ( $this, $file ) = @_;

    my $row = vtl_db->tmp_select_last_row;
    my $sth = $dbh->prepare($row);

    $sth->execute();
    my $id = $sth->fetchrow_hashref;

    return $id;
}

sub createTmpFileForResume {
    my ( $this, $path, $file, $line ) = @_;

    my $i        = 0;
    my $campagne = "TMP_" . $file;

    if ( -f $path . $campagne ) {
        die "ALREADY $campagne\n";
    }
    open( OUT, ">:encoding(UTF-8)", $path . $campagne ) || die "$!";

    open my $fh, '<:', $path . $file
        or die qq{Unable to open "$path . $file" for input: $!};

    while ( my $row = <$fh> ) {
        if ( $i >= $line ) {
            print OUT $row;
        }
        $i++;
    }

    close OUT;

    return $campagne;

}

########################################################
#
# Pour Les fonctions temporaires
#
########################################################

sub tmp_exportFreshData {
    my ($this) = @_;

    my $file = $this->tmp_exportDataFreshDateElig();
    print "[SEND] Send email \n";
    $this->sendEmailWithLinkSMTP( $file->{filename} );
    print "|-----> DONE \n";
}

sub tmp_insertDataSingleFile {
    my ( $this, $path, $file ) = @_;
    my $value;
    my $tmpInsertData = vtl_db->tmp_insert_table_bookers();
    $dbh->{'AutoCommit'} = 0;
    if ( $path || $file ) {
        my $errors;
        my $fh;
        my @values;
        my @dataBatch;
        my $i             = 0;
        my $dataInsert    = 0;
        my $nextUpdate    = 0;
        my $count         = 0;
        my $noField       = 0;
        my $batchSize     = 10000;
        my $total         = $this->countLinesCSV( $path . $file );
        my $checkValidRow = $this->countLinesCSV( $path . $file, undef, \1 );
        my $sth           = $dbh->prepare($tmpInsertData);

        my $csv = Text::CSV->new( { sep_char => "|", blank_is_undef => 1 } );

        if ( $total < $checkValidRow ) {
            open $fh, '<:raw:encoding(UTF-8)', $path . $file
                or die qq{Unable to open "$path . $file" for input: $!};

            $total = $checkValidRow;
            while ( my $row = <$fh> ) {
                if ( $noField > 0 ) {
                    chomp $row;
                    $row =~ s/"//g;
                    my @fields = [ split( /\|/, $row ) ];
                    push @dataBatch, @fields;
                }
                $noField++;
            }
        }
        else {
            open $fh, '<:', $path . $file
                or die qq{Unable to open "$path . $file" for input: $!};

            $csv->getline($fh);
            while ( my $row = $csv->getline($fh) ) {
                chomp $row;
                push @dataBatch, [ @$row[ 0 .. 38 ] ];
            }
        }

        print "\n";    #Si non la progresse bar se superpose

        my $progress_bar = Term::ProgressBar->new(
            (   {   ETA   => 'linear',
                    name  => "[INSERT] ",
                    count => $total
                }
            )
        );

        foreach my $enregistrement (@dataBatch) {
            push @values, $enregistrement;
            $count++;
            if (   $count % $batchSize == 0
                || $count == $total )
            {
                try {

                    $dataInsert = $count;

                    $sth->execute(@$_) for @values;
                    @values = ();

                }
                catch {
                    $errors .= "[$i] got dbi error: $_";
                };
            }
            $i++;
            if ( $i % 10000 eq 0 ) {
                $dbh->commit();
            }
            $nextUpdate = $progress_bar->update($i)
                if $i >= $nextUpdate;
        }

        $progress_bar->update($total)
            if $total >= $nextUpdate;

        $dbh->commit();
        $sth->finish;

        print
            "$dataInsert / $i nouvelles lignes sont enregistrees dans la BDD\n";

        if ($errors) {
            $errors .= "\n$dataInsert / $i, donnees insere.";
            $this->createErrorFile( $errors, $file );
        }

        return 0;
    }
    else {
        print "L'un des arguments -path ou -file n'est pas renseigné(s)\n";
    }
    return 0;
}

sub tmp_insertDataSingleFileFeedback {
    my ( $this, $path, $file ) = @_;
    my $value;
    my $tmpInsertData = vtl_db->tmp_insert_table_feedback();
    if ( $path || $file ) {
        my $errors;
        my $i          = 0;
        my $dataInsert = 0;
        my $nextUpdate = 0;
        my $count      = 0;
        my $batchSize  = 10000;
        my $total      = $this->countLinesCSV( $path . $file, \1 );
        if ( $total <= 0 ) {
            $total = $this->countLinesCSV( $path . $file, undef, \1 );
        }
        my @values;
        my @dataBatch;
        my $sth = $dbh->prepare($tmpInsertData);

        my $csv = Text::CSV->new( { sep_char => ";", blank_is_undef => 1 } );

        open my $fh, '<:raw:encoding(UTF-8)', $path . $file
            or die qq{Unable to open "$path . $file" for input: $!};

        $csv->getline($fh);

        print "\n";    #Si non la progresse bar se superpose

        my $progress_bar = Term::ProgressBar->new(
            (   {   ETA   => 'linear',
                    name  => "[INSERT] ",
                    count => $total
                }
            )
        );

        while ( my $row = $csv->getline($fh) ) {
            chomp $row;
            @$row[21] =~ s/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/$3-$2-$1/gm;
            @$row[40] =~ s/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/$3-$2-$1/gm;
            @$row[40] = @$row[40] . " " . @$row[41];
            splice @$row, 41, 1;
            push @dataBatch, [ @$row[ 0 .. 42 ] ];
        }

        foreach my $enregistrement (@dataBatch) {
            push @values, $enregistrement;
            $count++;
            if (   $count % $batchSize == 0
                || $count == $total )
            {
                try {

                    $dataInsert = $count;
                    $sth->execute(@$_) for @values;
                    @values = ();
                }
                catch {
                    $errors .= "[$i] got dbi error: $_";
                };
            }
            $i++;
            if ( $i % 10000 eq 0 ) {
                $dbh->commit();
            }
            $nextUpdate = $progress_bar->update($i)
                if $i >= $nextUpdate;
        }

        $dataInsert = $count;
        $sth->execute(@$_) for @values;

        $sth->finish;

        $progress_bar->update($total)
            if $total >= $nextUpdate;

        $dbh->commit();

        print
            "$dataInsert / $i nouvelles lignes sont enregistrees dans la BDD\n";

        if ($errors) {
            $errors .= "\n$dataInsert / $i, donnees insere.";
            $this->createErrorFile( $errors, $file );
        }

        return 0;
    }
    else {
        print "L'un des arguments -path ou -file n'est pas renseigné(s)\n";
    }
    return 0;
}

sub tmp_updateDataFromAdress {
    my ($this) = @_;
    print "1.1.2 Update table bookers from adresse\n";

    my $update = vtl_db->tmp_update_table_bookers_from_adresse();

    my $sth = $dbh->prepare($update);
    $sth->execute();
    $sth->finish;

    print "-----> DONE \n";
}

sub tmp_updateContactFromResil {
    my ($this) = @_;
    print "[STEP 1] - 1.1.3 | Mise a jour depuis la table resil \n";

    my $updateTmpResil = vtl_db->tmp_update_all_contact_from_resil();
    my $sth            = $dbh->prepare($updateTmpResil);
    $sth->execute();
    $sth->finish;

    print "-----> DONE \n";
}

sub tmp_topageClient {
    my ( $this, $all ) = @_;
    my ($rq,   $top,   $idContactSrc, $nom,   $prenom, $tel1,
        $tel2, $email, $idVoieAdr,    $insee, $cp
    ) = '';
    my $total;
    my $i          = 0;
    my $nextUpdate = 0;
    my $selectTopageClientUnset;
    if ($all) {
        $selectTopageClientUnset
            = vtl_db->tmp_select_topage_client_unset_all();
    }
    else {
        $selectTopageClientUnset = vtl_db->tmp_select_topage_client_unset();
    }
    my $selectTopageClientExist = vtl_db->select_topage_client_exist();
    my $updateTopageClient      = vtl_db->tmp_update_topage_client();

    my $sthSTCU = $dbh->prepare($selectTopageClientUnset);
    my $sthSTCE = $dbh_free->prepare($selectTopageClientExist);
    my $sthUTC  = $dbh->prepare($updateTopageClient);

    $sthSTCU->execute();
    $total = $sthSTCU->rows;

    my $progress_bar = Term::ProgressBar->new(
        (   {   ETA   => 'linear',
                name  => "[TOPAGE CLIENT] ",
                count => $total
            }
        )
    );

    while ( my $res = $sthSTCU->fetchrow_hashref() ) {

        $nom       = $res->{nom};
        $prenom    = $res->{prenom};
        $tel1      = $res->{tel1};
        $tel2      = $res->{tel2};
        $email     = $res->{email};
        $idVoieAdr = $res->{id_adr_free};
        $insee     = $res->{code_insee};
        $cp        = $res->{cp};

        $sthSTCE->execute( $nom, $prenom, $idVoieAdr );

        $top = $sthSTCE->fetchrow_hashref();

        if ($top) {
            $sthUTC->execute( $today, "FREE", $top->{email},
                $top->{tel_portable},
                $idVoieAdr, $top->{nom_abonne}, $top->{prenom_abonne} );
        }

        $i++;
        $nextUpdate = $progress_bar->update($i)
            if $i >= $nextUpdate;
    }

    $sthSTCU->finish;
    $sthUTC->finish;

    $progress_bar->update($total)
        if $total >= $nextUpdate;

    return;
}

sub tmp_setFlagFeedback {
    my ($this) = @_;

    # my $flagUpdate = vtl_db;

    my $updateFlagUnsetVente = vtl_db->tmp_update_flag_feedback_vente();
    my $updateFlagUnsetNoContact
        = vtl_db->tmp_update_flag_feedback_no_contact_anymore();
    my $uFlagNoContact = vtl_db->tmp_update_flag_feedback_no_contact();

    my $sthUFUV  = $dbh->prepare($updateFlagUnsetVente);
    my $sthUFUNC = $dbh->prepare($updateFlagUnsetNoContact);
    my $sthUFnc  = $dbh->prepare($uFlagNoContact);

    $sthUFUV->execute();
    $sthUFUNC->execute();
    $sthUFnc->execute();

    $sthUFUV->finish;
    $sthUFUNC->finish;
    $sthUFnc->finish;

    return;
}

sub tmp_exportDataFreshDateElig {
    my ($this) = @_;

    print "[EXPORT] all data fresh date elig 1 month\n";

    my $file;
    my $count      = 0;
    my $nextUpdate = 0;
    my $date       = Date::Simple->new();
    my $dateD8     = today();
    my $line;
    my $campagne   = "EXPORT_" . $dateD8->as_d8;
    my $pathExport = "/var/www/vaillante.proxad.net/campaign/";

    if ( -f $pathExport . $campagne ) {
        die "ALREADY $campagne\n";
    }
    open( OUT, ">:encoding(UTF-8)", $pathExport . $campagne ) || die "$!";

    my ($id_contact_src,                  $src,
        $type_src,                        $code_campagne,
        $civ,                             $nom,
        $prenom,                          $adr1,
        $adr2,                            $adr3,
        $adr4,                            $id_adr_free,
        $cp,                              $ville,
        $pays,                            $tel1,
        $tel2,                            $code_insee,
        $hexacle,                         $date_eligibilite_ftth,
        $nro,                             $libelle_offre_mobile,
        $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
        $email,                           $top_client,
        $date_top_client,                 $orig_top_client,
        $optin_appel,                     $optin_sms,
        $optin_courrier,                  $optin_email,
        $operateur,                       $dt_deb_droit_adr,
        $dt_fin_droit_adr,                $dt_deb_droit_tel1,
        $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
        $dt_fin_droit_tel2,               $dt_deb_droit_email,
        $dt_fin_droit_email,              $id_rgpd,
        $techno
    ) = "";

    print OUT
        uc(
        "id_contact_src|src|type_src|code_campagne|civ|nom|prenom|adr1|adr2|adr3|adr4|id_adr_free|cp|ville|pays|tel1|tel2|code_insee|hexacle|date_eligibilite_ftth|nro|libelle_offre_mobile|date_de_creation_contrat_mobile|anciennete_eli_ftth|email|top_client|date_top_client|orig_top_client|optin_appel|optin_sms|optin_courrier|optin_email|operateur|dt_deb_droit_adr|dt_fin_droit_adr|dt_deb_droit_tel1|dt_fin_droit_tel1|dt_deb_droit_tel2|dt_fin_droit_tel2|dt_deb_droit_email|dt_fin_droit_email|id_rgpd|techno"
        ) . "\n";

    my $query = vtl_db->tmp_select_all_data_fresh_date_elig();

    my $sth = $dbh->prepare($query);

    $sth->execute();
    $sth->bind_columns(
        \(  $id_contact_src,                  $src,
            $type_src,                        $code_campagne,
            $civ,                             $nom,
            $prenom,                          $adr1,
            $adr2,                            $adr3,
            $adr4,                            $id_adr_free,
            $cp,                              $ville,
            $pays,                            $tel1,
            $tel2,                            $code_insee,
            $hexacle,                         $date_eligibilite_ftth,
            $nro,                             $libelle_offre_mobile,
            $date_de_creation_contrat_mobile, $anciennete_eli_ftth,
            $email,                           $top_client,
            $date_top_client,                 $orig_top_client,
            $optin_appel,                     $optin_sms,
            $optin_courrier,                  $optin_email,
            $operateur,                       $dt_deb_droit_adr,
            $dt_fin_droit_adr,                $dt_deb_droit_tel1,
            $dt_fin_droit_tel1,               $dt_deb_droit_tel2,
            $dt_fin_droit_tel2,               $dt_deb_droit_email,
            $dt_fin_droit_email,              $id_rgpd,
            $techno
        )
    );

    my $total        = $sth->rows;
    my $progress_bar = Term::ProgressBar->new(
        (   {   ETA   => 'linear',
                name  => "[DUMP] ",
                count => $total
            }
        )
    );

    while ( $sth->fetch() ) {

        $line
            = "$id_contact_src|$src|$type_src|$code_campagne|$civ|$nom|$prenom|$adr1|$adr2|$adr3|$adr4|$id_adr_free|$cp|$ville|$pays|$tel1|$tel2|$code_insee|$hexacle|$date_eligibilite_ftth|$nro|$libelle_offre_mobile|$date_de_creation_contrat_mobile|$anciennete_eli_ftth|$email|$top_client|$date_top_client|$orig_top_client|$optin_appel|$optin_sms|$optin_courrier|$optin_email|$operateur|$dt_deb_droit_adr|$dt_fin_droit_adr|$dt_deb_droit_tel1|$dt_fin_droit_tel1|$dt_deb_droit_tel2|$dt_fin_droit_tel2|$dt_deb_droit_email|$dt_fin_droit_email|$id_rgpd|$techno\n";

        print OUT $line;

        $count++;
        $nextUpdate = $progress_bar->update($count)
            if $count >= $nextUpdate;

    }

    close OUT;
    $count++;

    rename $pathExport . $campagne,
        $pathExport . $campagne . "_V1_$count.csv";
    $progress_bar->update($total)
        if $total >= $nextUpdate;

    $file = $campagne . "_V1_$count.csv";

    $sth->finish;

    print "-----> DONE \n";

    return { filename => $file };

}

sub list {
    my ($this) = @_;
    print "launchAllSteps \n";
    print
        "step1 (insertData, truncate, createResilProspectsFile, insertResilProspects) \n";
    print
        "step2 (normalize, insertDataSingleFileElig, drop table load_ref_eligibilite, create table load_ref_eligibilite, insert ref_eligibilite from anoter table, drop table load_ref_eligibilite)\n";
    print "insertData \n";
    print "insertData -path path_de_mon_fichier \n";
    print
        "insertDataSingleFile -path path_de_mon_fichier -file nom_de_mon_fichier \n";
    print "createResilProspectsFile -path destination_du_dump \n";
    print
        "updateDataFromAdress mets à jour les données de la table contact via la correspondance de l'hexacle dans la table adresse \n";
}

1;
