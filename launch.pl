#! /usr/bin/perl -w

use strict;
use warnings;
use utf8;
use Text::CSV;
use IO::Handle;
use Data::Dumper;

use 5.20.0;
use experimental 'signatures';

use lib './modules/';

use db;
use file;

STDOUT->autoflush;

my ( $path, $file, @regFiles, %fns, @files, $pathResil, $isResume );
my $pathRefElig = "/home/hossen/ref_eligibilite/";
my $function    = shift;

foreach my $arg (@ARGV) {
    if ( $arg =~ /.*\.csv$/ ) {
        $file = $arg;
    }
    if ( $arg =~ /^.*(?:\\|\/)(?!.*(?:\\|\/))(.*)$/gm ) {
        $path = $arg;
    }
    if ( $arg eq "resume" ) {
        $isResume = \1;
    }
}

if ( not defined $file ) {
    if ($path) {
        opendir my $dir, $path
            or die "Cannot open directory: $!";
        @files = grep {/^(?!DONE_).*\.csv$/i} readdir $dir;
        closedir $dir;
    }
    else {
        opendir my $dir, "/home/hossen/IN/"
            or die "Cannot open directory: $!";
        @files = grep {/^(?!DONE_).*\.csv$/i} readdir $dir;
        closedir $dir;
    }
}

if ( not defined $path ) {
    $path = "/home/hossen/IN/";
}

if ( $function eq "step2" ) {

    opendir my $dirRefElig, $pathRefElig
        or die "Cannot open directory: $!";
    @regFiles = grep {/.*\.csv$/} readdir $dirRefElig;
    closedir $dirRefElig;
}

if ( $function eq "insertResilProspects" ) {
    $pathResil = $path . $file;
}

%fns = (
    insertData               => [ $path,        \@files ],
    insertDataSingleFile     => [ $path,        $file ],
    insertDataSingleFileElig => [ $pathRefElig, $file ],
    insertResilProspects     => [$pathResil],
    createResilProspectsFile => [$path],
    step1                    => [ $path, \@files, $file, $isResume ]
    , #step1 insert inside table tmp and get anything about the contact informations (more optimize)
    step1NotOptimize => [ $path, \@files, $file ]
    ,    #same from step1 but insert inside directly table contact (too slow)
    step2 => [ \@regFiles ]
    ,    #step for insert in table ref_elig, anything about new eligibility
    step3                    => [ $path, $file ],    #step for resil table
    step4                    => [ $path, \@files, $file ],  #step for feedback
    resumeInsert             => [ $path, $file ],
    fileProcessing           => [ $path, $file ],
    renameFile               => [ $path, $file ],
    insertFile               => [ $path, $file ],
    tmp_insertDataSingleFile => [ $path, $file ],
    tmp_topageClient             => [$isResume],
    tmp_updateDataFromAdress     => undef,
    tmp_updateContactFromResil   => undef,
    tmp_exportFreshData          => undef,
    exportDataTestWithCopyToFTP  => undef,
    updateFile                   => undef,
    updateDataWithIdFile         => undef,
    historyAndCleaning           => undef,
    exportGroup                  => undef,
    exportFreshData              => undef,
    exportFreshDataFromEligTable => undef,
    exportDataGroupByCP          => undef,
    list                         => undef
);

if ( defined $function ) {
    if ( exists $fns{$function} ) {
        vtl_file->$function(
            $fns{$function}[0],
            $fns{$function}[1],
            $fns{$function}[2]
        );
    }
    else {
        die
            "Aucune fonction $function existe, pour savoir la liste des fonctions, veuillez taper -> perl launch.pl list\n";
    }
}
else {
    die
        "Veuillez appeler une fonction presente dans la liste. Pour savoir la liste des fonctions, veuillez taper -> perl launch.pl list\n";
}
