#! /bin/bash

DATE=`date -d '1 day ago' +%Y%m%d`
TODAY=`date -d 'today' +%Y%m%d`

# CHEMIN="/var/www/html/ref_eligibilite"
# mkdir "/var/www/html/ref_eligibilite"

CHEMIN="/home/hossen/ref_eligibilite"

if [ ! -d "$CHEMIN" ]; then
	echo "Create ref_eligibilite folder."
	mkdir "/home/hossen/ref_eligibilite"
	echo "-------------------> DONE"
	echo \n
fi

echo "Copy start."
scp -r hossen@172.20.241.29:/root/degroup/fibre/adresses_elig_${DATE}_08.csv /home/hossen/ref_eligibilite/adresses_elig_week.csv
scp -r hossen@172.20.241.29:/root/degroup/fibre/adresses_elig_raco_${DATE}_08.csv /home/hossen/ref_eligibilite/adresses_elig_raco_week.csv
echo "-------------------> DONE"
echo \n

# scp -r hossen@172.20.241.29:/root/degroup/fibre/adresses_elig_${DATE}_08.csv /home/hossen/ref_eligibilite/adresses_elig_week.csv # PROD
# scp -r hossen@172.20.241.29:/root/degroup/fibre/adresses_elig_raco_${DATE}_08.csv /home/hossen/ref_eligibilite/adresses_elig_raco_week.csv # PROD

echo "Normalize files << adresses_elig_week >> and << adresses_elig_raco_week >> ."
perl scripts/normalize.pl /home/hossen/ref_eligibilite/adresses_elig_week.csv > /home/hossen/ref_eligibilite/adresses_elig_week.N.csv
perl scripts/normalize.pl /home/hossen/ref_eligibilite/adresses_elig_raco_week.csv > /home/hossen/ref_eligibilite/adresses_elig_raco_week.N.csv
echo "-------------------> DONE"
echo \n

echo "Copy on server << adresses_elig_week >> and << adresses_elig_raco_week >> ."
scp -r /home/hossen/ref_eligibilite/adresses_elig_week.N.csv hossen@vaillante:/home/hossen/ref_eligibilite/ # PROD
scp -r /home/hossen/ref_eligibilite/adresses_elig_raco_week.N.csv hossen@vaillante:/home/hossen/ref_eligibilite/ # PROD
echo "-------------------> DONE"
echo \n

# echo "Insert files << adresses_elig_week >> and << adresses_elig_raco_week >> ."
# perl launch.pl insertDataSingleFileElig -file adresses_elig_week.N.csv
# perl scripts/insertDataSingleFileElig.pl /home/hossen/ref_eligibilite/adresses_elig_week.N.csv
# perl scripts/insertDataSingleFileElig.pl /home/hossen/ref_eligibilite/adresses_elig_raco_week.N.csv
# echo \n
# echo "-------------------> DONE"

# echo "Delete useless files."
# rm -v $CHEMIN/adresses_elig_week.csv
# rm -v $CHEMIN/adresses_elig_raco_week.csv
# echo "-------------------> DONE"
# echo \n

# echo "launch all functions."
# perl launch.pl launchAllSteps
# perl launch.pl step2
# echo "-------------------> DONE"
# echo \n