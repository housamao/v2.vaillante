#! /bin/bash

echo "[VAILLANTE SCRIPTS] -> launch_ref_eligibilite START"

TYPE=
DATE=`date -d '1 day ago' +%Y%m%d`
TODAY=`date -d 'today' +%Y%m%d`
CHEMIN="/home/hossen/ref_eligibilite"

if [ $1 ]; then
	if [[ $1 == "date" ]]; then
		TYPE=$DATE;
	elif  [[ $1 == "today" ]]; then
		TYPE=$TODAY
	else
		TYPE=$TODAY
	fi
else
	TYPE=$TODAY
fi


if [ ! -d "$CHEMIN" ]; then
	echo "Create ref_eligibilite folder."
	mkdir "/home/hossen/ref_eligibilite"
	echo "-------------------> DONE"
	echo \n
fi

echo "Copy on server << adresses_elig_week >> and << adresses_elig_raco_week >>"
scp -r hossen@172.20.241.29:/root/degroup/fibre/adresses_elig_${TYPE}_08.csv $CHEMIN/adresses_elig_week.csv
scp -r hossen@172.20.241.29:/root/degroup/fibre/adresses_elig_raco_${TYPE}_08.csv $CHEMIN/adresses_elig_raco_week.csv
echo "-------------------> DONE"
echo \n

echo "Normalize files << adresses_elig_week >> and << adresses_elig_raco_week >>"
perl /home/hossen/v2.vaillante/scripts/normalize.pl $CHEMIN/adresses_elig_week.csv > $CHEMIN/adresses_elig_week.N.csv
perl /home/hossen/v2.vaillante/scripts/normalize.pl $CHEMIN/adresses_elig_raco_week.csv > $CHEMIN/adresses_elig_raco_week.N.csv
echo "-------------------> DONE"
echo \n

echo "Delete files not normalized << adresses_elig_week >> and << adresses_elig_raco_week >>"
rm $CHEMIN/adresses_elig_week.csv
rm $CHEMIN/adresses_elig_raco_week.csv
echo "-------------------> DONE"
echo \n

echo "Launch scripts"
perl /home/hossen/v2.vaillante/launch.pl step2
perl /home/hossen/v2.vaillante/launch.pl exportFreshDataFromEligTable
echo "-------------------> DONE"
echo \n

echo "[VAILLANTE SCRIPTS] -> launch_ref_eligibilite FINISH"
echo \n