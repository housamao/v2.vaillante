#! /bin/bash

echo "[VAILLANTE SCRIPTS] -> Traitements des données depuis les fichiers brookers START"

while getopts f:p: option;
do
	case "${option}" in
		f) file=${OPTARG};;
		p) path=${OPTARG};;
	esac
done


if [ ! "$path" ]; then
	path="/home/hossen/IN/"
fi


if [ ! "$file" ]; then

	filesArr=`ls ${path}*.csv` 
	
	for csv in ${filesArr}
		do 
			perl /home/hossen/v2.vaillante/launch.pl insertFile -file ${csv#${path}} -path ${path} 
			perl /home/hossen/v2.vaillante/launch.pl tmp_insertDataSingleFile -file ${csv#${path}} -path ${path} 
			perl /home/hossen/v2.vaillante/launch.pl tmp_updateDataFromAdress
			perl /home/hossen/v2.vaillante/launch.pl tmp_updateContactFromResil
			perl /home/hossen/v2.vaillante/launch.pl tmp_topageClient

			echo "[STEP 1] - 1.2 | Mise a jour des contacts en rattachant a un id file"
			perl /home/hossen/v2.vaillante/launch.pl updateDataWithIdFile
			echo "1.2 |-----> DONE"
			echo \n

			echo "[STEP 1] - 1.3 | Mise a jour de la table file "
			perl /home/hossen/v2.vaillante/launch.pl updateFile
			echo "1.3 |-----> DONE"
			echo \n

			perl /home/hossen/v2.vaillante/launch.pl fileProcessing -file ${csv#${path}} -path ${path}
			echo \n


			echo "[STEP 1] | Copie dans le dossier archive"
			perl /home/hossen/v2.vaillante/launch.pl copyFileToArchiveFolder -path ${path} -file ${csv#${path}}
			echo "|-----> DONE"
			echo \n

			echo "[STEP 1] | Renomme le fichier"
			perl /home/hossen/v2.vaillante/launch.pl renameFile -path ${path} -file ${csv#${path}}
			echo "|-----> DONE"
			echo \n


			perl /home/hossen/v2.vaillante/launch.pl historyAndCleaning -path ${path} -file ${csv#${path}}

			echo "${csv#${path}} |-----> DONE"
	done
else
	perl /home/hossen/v2.vaillante/launch.pl insertFile -file ${file} -path ${path} 
	perl /home/hossen/v2.vaillante/launch.pl tmp_insertDataSingleFile -file ${file} -path ${path} 
	perl /home/hossen/v2.vaillante/launch.pl tmp_updateDataFromAdress
	perl /home/hossen/v2.vaillante/launch.pl tmp_updateContactFromResil
	perl /home/hossen/v2.vaillante/launch.pl tmp_topageClient

	echo "[STEP 1] - 1.2 | Mise a jour des contacts en rattachant a un id file"
	perl /home/hossen/v2.vaillante/launch.pl updateDataWithIdFile
	echo "1.2 |-----> DONE"
	echo \n

	echo "[STEP 1] - 1.3 | Mise a jour de la table file "
	perl /home/hossen/v2.vaillante/launch.pl updateFile
	echo "1.3 |-----> DONE"
	echo \n

	perl /home/hossen/v2.vaillante/launch.pl fileProcessing -file ${file} -path ${path}
	echo \n


	echo "[STEP 1] | Copie dans le dossier archive"
	perl /home/hossen/v2.vaillante/launch.pl copyFileToArchiveFolder -path ${path} -file ${file}
	echo "|-----> DONE"
	echo \n

	echo "[STEP 1] | Renomme le fichier"
	perl /home/hossen/v2.vaillante/launch.pl renameFile -path ${path} -file ${file}
	echo "|-----> DONE"
	echo \n


	perl /home/hossen/v2.vaillante/launch.pl historyAndCleaning -path ${path} -file ${file}

	echo "${file} |-----> DONE"
fi